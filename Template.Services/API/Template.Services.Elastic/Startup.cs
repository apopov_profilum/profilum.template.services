﻿using AutoMapper;
using CorrelationId;
using Serilog;
using Swashbuckle.AspNetCore.SwaggerGen;
using System;
using System.IO;
using System.Linq;
using System.Reflection;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Options;
using Nest;
using Template.Services.Core.Swagger.OperationFilters;
using Template.Services.Core.Swagger.DocumentFilters;
using Template.Services.Core.Swagger;
using Template.Services.Core.Logging.SerilogEnrichers;
using Template.Services.Elastic.Models;
using Template.Services.Elastic.DataAccess;
using Template.Services.Elastic.DataAccess.Interfaces;
using Template.Services.Elastic.Config;
using Template.Services.Elastic.Infrastructure;

namespace Profilum.Services.Content
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddCorrelationId();

            services.Configure<ElasticServiceSettings>(Configuration.GetSection(nameof(ElasticServiceSettings)));
            // var contentConfig = Configuration.GetSection(nameof(ElasticServiceSettings)).Get<ElasticServiceSettings>();

            var elasticSettings = new ConnectionSettings(new Uri(Configuration.GetConnectionString("ElasticConnection")))
                .DefaultMappingFor<ElasticDocument>(m => m.IndexName("template"));

            services.AddTransient<IElasticClient, ElasticClient>(provider => new ElasticClient(elasticSettings));
            services.AddTransient<IElasticRecordContext, ElasticRecordContext>();
                // provider => new ElasticRecordContext(provider.GetService<ILogger>(), provider.GetService<IElasticClient>()));

            services.AddMvc().SetCompatibilityVersion(CompatibilityVersion.Version_2_2);

            services.AddAutoMapper(Assembly.GetEntryAssembly());
            services.AddRouting(options => { options.LowercaseUrls = true; options.AppendTrailingSlash = false; });

            services.AddApiVersioning(o => o.ReportApiVersions = true);

            // DI for Handlers and Managers
            services.AddBusinessLogic();

            services.RegisterEasyNetQ(Configuration.GetConnectionString("RabbitMqConnection"));

            var logger = new LoggerConfiguration()
                .Enrich.With(new CorrelationIdEnricher(services.BuildServiceProvider().GetService<ICorrelationContextAccessor>()))
                .ReadFrom.Configuration(Configuration)
                .CreateLogger();
            services.AddSingleton<Serilog.ILogger>(logger);

            services.AddLogging(builder => builder.AddSerilog(logger));

            services.AddSwaggerGen(c =>
            {
                c.SwaggerDoc("v1.0", SwaggerApiInfoGetter.CreateInfoForApiVersion("v1.0", "Elastic"));

                c.DocInclusionPredicate((docName, apiDesc) =>
                {
                    if (!apiDesc.TryGetMethodInfo(out MethodInfo methodInfo)) return false;

                    var versions = methodInfo.DeclaringType
                        .GetCustomAttributes(true)
                        .OfType<ApiVersionAttribute>()
                        .SelectMany(attr => attr.Versions);

                    return versions.Any(v => $"v{v.ToString()}" == docName);
                });

                c.OperationFilter<RemoveVersionOperationFilter>();
                c.OperationFilter<CorrelationFieldsOperationFilter>();

                c.DocumentFilter<SetVersionInPathDocumentFilter>();
                c.DocumentFilter<LowercaseDocumentFilter>();

                c.IncludeXmlComments(Path.Combine($"{AppContext.BaseDirectory}", $"{Assembly.GetEntryAssembly().GetName().Name}.xml"));
                c.DescribeAllEnumsAsStrings();
            });
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            app.UseCorrelationId(new CorrelationIdOptions
            {
                UseGuidForCorrelationId = true,
                UpdateTraceIdentifier = false
            });

            app.UseMvc();
            app.ConfigureEventBus();

            app.UseSwagger(c =>
            {
                c.RouteTemplate = "api/docs/{documentName}/swagger.json";
            });

            if (env.IsDevelopment())
            {
                app.UseSwaggerUI(c =>
                {
                    c.RoutePrefix = "api/docs";

                    c.SwaggerEndpoint("/api/docs/v1.0/swagger.json", "v1.0");
                });
            }
        }
    }
}