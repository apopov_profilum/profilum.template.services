﻿using Template.Services.Core.Constants;
using Template.Services.Dtos.Base.Requests;
using Template.Services.Dtos.Base.Responses;
using Template.Services.Dtos.Elastic.Requests;
using Template.Services.Dtos.Elastic.Responses;
using Template.Services.Elastic.Handlers.Interfaces;
using Template.Services.Elastic.Managers.Interfaces;
using System;
using System.Threading.Tasks;

namespace Template.Services.Elastic.Handlers
{
    public class ElasticHandler : IElasticHandler
    {
        private readonly IElasticManager _elasticManager;

        public ElasticHandler(IElasticManager elasticManager)
        {
            _elasticManager = elasticManager;
        }

        public async Task<CreateElasticDocumentResponse> CreateAsync(CreateElasticDocumentRequest request) => await HandleActionAsync(request, (r) => _elasticManager.CreateAsync(request));

        public async Task<GetElasticDocumentResponse> GetOneAsync(GetElasticDocumentRequest request) => await HandleActionAsync(request, (r) => _elasticManager.GetOneAsync(request));

        public async Task<GetElasticDocumentsResponse> GetSomeAsync(GetElasticDocumentsRequest request) => await HandleActionAsync(request, (r) => _elasticManager.GetSomeAsync(request));

        public async Task<UpdateElasticDocumentResponse> UpdateAsync(UpdateElasticDocumentRequest request) => await HandleActionAsync(request, (r) => _elasticManager.UpdateAsync(request));

        public async Task<DeleteElasticDocumentResponse> DeleteAsync(DeleteElasticDocumentRequest request) => await HandleActionAsync(request, (r) => _elasticManager.DeleteAsync(request));


        /// <summary>
        /// Base method to handle action
        /// </summary>
        /// <typeparam name="TRequest">Request type</typeparam>
        /// <typeparam name="TResponse">Response type</typeparam>
        /// <param name="request"></param>
        /// <param name="action"></param>
        /// <returns></returns>
        private async Task<TResponse> HandleActionAsync<TRequest, TResponse>(TRequest request, Func<TRequest, Task<TResponse>> action) where TRequest : BaseRequest, new()
                                                                                                                                       where TResponse : BaseResponse, new()
        {
            try
            {
                return await action(request);
            }
            catch (Exception ex)
            {
                return new TResponse()
                {
                    Comment = ex.Message,
                    Status = ResponseStatus.Failed
                };
            }
        }
    }
}
