﻿using Template.Services.Dtos.Elastic.Requests;
using Template.Services.Dtos.Elastic.Responses;
using System.Threading.Tasks;

namespace Template.Services.Elastic.Handlers.Interfaces
{
    public interface IElasticHandler
    {
        Task<CreateElasticDocumentResponse> CreateAsync(CreateElasticDocumentRequest request);

        Task<GetElasticDocumentResponse> GetOneAsync(GetElasticDocumentRequest request);

        Task<GetElasticDocumentsResponse> GetSomeAsync(GetElasticDocumentsRequest request);

        Task<UpdateElasticDocumentResponse> UpdateAsync(UpdateElasticDocumentRequest request);

        Task<DeleteElasticDocumentResponse> DeleteAsync(DeleteElasticDocumentRequest request);
    }
}
