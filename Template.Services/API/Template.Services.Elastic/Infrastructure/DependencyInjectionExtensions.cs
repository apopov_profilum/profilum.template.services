﻿using Microsoft.Extensions.DependencyInjection;
using Template.Services.Elastic.Handlers;
using Template.Services.Elastic.Handlers.Interfaces;
using Template.Services.Elastic.Managers;
using Template.Services.Elastic.Managers.Interfaces;

namespace Template.Services.Elastic.Infrastructure
{
    public static class DependencyInjectionExtentions
    {
        public static void AddBusinessLogic(this IServiceCollection services)
        {
            services.AddTransient<IElasticHandler, ElasticHandler>();
            services.AddTransient<IElasticManager, ElasticManager>();
        }
    }
}
