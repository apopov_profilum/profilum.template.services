﻿using Microsoft.AspNetCore.Builder;
using Microsoft.Extensions.DependencyInjection;
using Template.Services.Dtos.Elastic.Requests;
using Template.Services.Elastic.Managers.Interfaces;

namespace Template.Services.Elastic.Infrastructure
{
    public static class RabbitMqSubscriptionExtensions
    {
        public static void ConfigureEventBus(this IApplicationBuilder builder)
        {
            var eventBus = builder.ApplicationServices.GetRequiredService<EasyNetQ.IBus>();

            eventBus.RespondAsync(async (CreateElasticDocumentRequest request) => await builder.ApplicationServices.CreateScope().ServiceProvider.GetService<IElasticManager>().CreateAsync(request));
            eventBus.RespondAsync(async (GetElasticDocumentRequest request) => await builder.ApplicationServices.CreateScope().ServiceProvider.GetService<IElasticManager>().GetOneAsync(request));
            eventBus.RespondAsync(async (GetElasticDocumentsRequest request) => await builder.ApplicationServices.CreateScope().ServiceProvider.GetService<IElasticManager>().GetSomeAsync(request));
            eventBus.RespondAsync(async (UpdateElasticDocumentRequest request) => await builder.ApplicationServices.CreateScope().ServiceProvider.GetService<IElasticManager>().UpdateAsync(request));
            eventBus.RespondAsync(async (DeleteElasticDocumentRequest request) => await builder.ApplicationServices.CreateScope().ServiceProvider.GetService<IElasticManager>().DeleteAsync(request));
        }
    }
}
