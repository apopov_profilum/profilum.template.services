﻿namespace Template.Services.Elastic.Config
{
    public class ElasticServiceSettings
    {
        /// <summary>
        /// Имя индекса (базы данных) Elastic
        /// </summary>
        public string DefaultIndexName { get; set; }

        /// <summary>
        /// Максимальное количество строк для выдачи в запросе в БД
        /// </summary>
        public int MaxRowsCount { get; set; }
    }
}
