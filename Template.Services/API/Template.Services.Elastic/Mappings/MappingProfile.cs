﻿using AutoMapper;
using Template.Services.Dtos.Elastic.Dto;
using Template.Services.Elastic.Models;

namespace Template.Services.Elastic.Mappings
{
    public class MappingProfile : Profile
    {
        public MappingProfile()
        {
            CreateMap<ElasticDocument, ElasticDocumentDto>().ReverseMap();
        }
    }
}
