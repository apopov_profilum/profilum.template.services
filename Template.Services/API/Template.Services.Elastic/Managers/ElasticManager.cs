﻿using AutoMapper;
using Template.Services.Dtos.Elastic.Requests;
using Template.Services.Dtos.Elastic.Responses;
using Template.Services.Elastic.DataAccess.Interfaces;
using Template.Services.Elastic.Managers.Interfaces;
using Serilog;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Template.Services.Elastic.Models;
using Template.Services.Dtos.Elastic.Dto;
using Template.Services.Core.Exceptions;
using Template.Services.Core.Constants;

namespace Template.Services.Elastic.Managers
{
    public class ElasticManager : IElasticManager
    {
        private readonly IElasticRecordContext _db;
        private readonly ILogger _logger;
        private readonly IMapper _mapper;

        public ElasticManager(IElasticRecordContext db,
                              ILogger logger,
                              IMapper mapper)
        {
            _db = db;
            _logger = logger;
            _mapper = mapper;
        }

        public async Task<CreateElasticDocumentResponse> CreateAsync(CreateElasticDocumentRequest request)
        {
            var doc = _mapper.Map<ElasticDocumentDto, ElasticDocument>(request.Document, opt => opt.AfterMap((src, dest) =>
            {
                dest.CreatedDate = DateTime.UtcNow;
                dest.Id = src.Id != Guid.Empty ? src.Id : Guid.NewGuid();
            }));

            await _db.AddAsync<ElasticDocument>(doc);

            return new CreateElasticDocumentResponse
            {
                Status = ResponseStatus.Success
            };
        }

        public async Task<DeleteElasticDocumentResponse> DeleteAsync(DeleteElasticDocumentRequest request)
        {
            await _db.DeleteAsync<ElasticDocument>(request.Id);
            return new DeleteElasticDocumentResponse
            {
                Status = ResponseStatus.Success
            };
        }

        public async Task<GetElasticDocumentResponse> GetOneAsync(GetElasticDocumentRequest request)
        {
            try
            {
                var doc = await _db.GetByIdAsync<ElasticDocument>(request.Id);
                return new GetElasticDocumentResponse
                {
                    Document = _mapper.Map<ElasticDocument, ElasticDocumentDto>(doc),
                    Status = ResponseStatus.Success
                };
            }
            catch (EntityNotFoundException nfex)
            {
                _logger.Error(nfex, nfex.Message);
                return new GetElasticDocumentResponse
                {
                    Status = ResponseStatus.NotFound
                };
            }
        }

        public async Task<GetElasticDocumentsResponse> GetSomeAsync(GetElasticDocumentsRequest request)
        {
            var docs = await _db.GetAllAsync<ElasticDocument>(10);
            return new GetElasticDocumentsResponse
            {
                Status = ResponseStatus.Success,
                Documents = docs.Select(d => _mapper.Map<ElasticDocument, ElasticDocumentDto>(d)).ToList()
            };
        }

        public async Task<UpdateElasticDocumentResponse> UpdateAsync(UpdateElasticDocumentRequest request)
        {
            var doc = _mapper.Map<ElasticDocumentDto, ElasticDocument>(request.Document);

            await _db.UpdateAsync<ElasticDocument>(doc);

            return new UpdateElasticDocumentResponse
            {
                Status = ResponseStatus.Success
            };
        }
    }
}
