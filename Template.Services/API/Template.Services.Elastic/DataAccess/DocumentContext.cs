﻿using Nest;
using Template.Services.Core.Exceptions;
using Template.Services.Core.Utilities;
using Template.Services.Elastic.DataAccess.Interfaces;
using Serilog;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Template.Services.Elastic.DataAccess
{
    /// <summary>
    /// Релизация доступа к документо-ориентированной БД
    /// </summary>
    public class DocumentContext : IDocumentContext
    {
#pragma warning disable CS1591 // Missing XML comment for publicly visible type or member
        protected readonly ILogger _logger;
        protected readonly IElasticClient _client;

        private const string NotFoundByHridException = "Can't find entity by hrid {0}";
        private const string NotFoundByIdException = "Can't find entity by id {0}";
        private const string NotFoundByIdsException = "Can't find entity by ids: {0}";
        private const string NotFoundByConditionException = "Can't find entity by condition";
        private const string InternalError = "Internal error";
        private const string HridAlreadyExists = "Hrid {0} already exists";
        private const string UndefinedOperationType = "Undefined operation type";
        private const string IncorrectId = "Incorrect Id value";

        private const int PartitionSize = 100;

        public DocumentContext(ILogger logger, IElasticClient client)
#pragma warning restore CS1591 // Missing XML comment for publicly visible type or member
        {
            _logger = logger;
            _client = client;
        }

        /// <summary>
        /// Добавить документ
        /// </summary>
        /// <typeparam name="T">Тип сущности</typeparam>
        /// <param name="value">Добавляемое значение</param>
        public void Add<T>(T value) where T : class
        {
            var response = _client.Index<T>(value, i => i.Index<T>());

            if (!response.IsValid || response.Result == Result.Error)
            {
                _logger.Error(InternalError);
                throw new InvalidOperationException(InternalError);
            }
        }

        /// <summary>
        /// Добавить документ
        /// </summary>
        /// <typeparam name="T">Тип сущности</typeparam>
        /// <param name="value">Добавляемое значение</param>
        public async Task AddAsync<T>(T value) where T : class
        {
            var response = await _client.IndexAsync<T>(value, i => i.Index<T>());

            if (!response.IsValid || response.Result == Result.Error)
            {
                _logger.Error(InternalError);
                throw new InvalidOperationException(InternalError);
            }
        }

        /// <summary>
        /// Обновить документ
        /// </summary>
        /// <typeparam name="T">Тип сущности</typeparam>
        /// <param name="value">Добавляемое значение</param>
        public void Update<T>(T value) where T : class
        {
            var response = _client.Index<T>(value, i => i.Index<T>());

            if (!response.IsValid || response.Result == Result.Error)
            {
                _logger.Error(InternalError);
                throw new InvalidOperationException(InternalError);
            }
        }

        /// <summary>
        /// Обновить документ асинхронно
        /// </summary>
        /// <typeparam name="T">Тип сущности</typeparam>
        /// <param name="value">Добавляемое значение</param>
        public async Task UpdateAsync<T>(T value) where T : class
        {
            var response = await _client.IndexAsync<T>(value, i => i.Index<T>());

            if (!response.IsValid || response.Result == Result.Error)
            {
                _logger.Error(InternalError);
                throw new InvalidOperationException(InternalError);
            }
        }

        /// <summary>
        /// Получить данные по идентификатору документа
        /// </summary>
        /// <typeparam name="T">Тип сущности</typeparam>
        /// <param name="id">Идетификатор документа</param>
        /// <returns></returns>
        public T GetById<T>(Guid id) where T : class
        {
            var response = _client.Search<T>(s => s.Query(q => q.Ids(c => c.Values(id))));
            if (!response.IsValid)
            {
                _logger.Error(InternalError);
                throw new InvalidOperationException(InternalError);
            }
            return response.Documents != null && response.Documents.Any() ? response.Documents.First()
                                                                          : throw new EntityNotFoundException(string.Format(NotFoundByIdException, id));
        }

        /// <summary>
        /// Получить данные по идентификатору документа
        /// </summary>
        /// <typeparam name="T">Тип сущности</typeparam>
        /// <param name="id">Идетификатор документа</param>
        /// <returns></returns>
        public async Task<T> GetByIdAsync<T>(Guid id) where T : class
        {
            var response = await _client.SearchAsync<T>(s => s.Query(q => q.Ids(c => c.Values(id))));
            if (!response.IsValid)
            {
                _logger.Error(InternalError);
                throw new InvalidOperationException(InternalError);
            }
            return response.Documents != null && response.Documents.Any() ? response.Documents.First()
                                                                          : throw new EntityNotFoundException(string.Format(NotFoundByIdException, id));
        }

        /// <summary>
        /// Получить данные по нескольким документам
        /// </summary>
        /// <typeparam name="T">Тип сущности</typeparam>
        /// <param name="ids">Идетификаторы документов</param>
        /// <returns></returns>
        public IEnumerable<T> GetByIds<T>(ICollection<Guid> ids) where T : class
        {
            var elements = new List<T>();
            if (ids.Count > PartitionSize)
            {
                var partitions = ids.Partition(PartitionSize);
                foreach (var partition in partitions)
                {
                    var response = _client.Search<T>(s => s.Size(ids.Count).Query(q => q.Ids(c => c.Values(partition))));
                    if (!response.IsValid)
                    {
                        _logger.Error(InternalError);
                        throw new InvalidOperationException(InternalError);
                    }
                    if (response.Documents != null && response.Documents.Any())
                    {
                        elements.AddRange(response.Documents);
                    }
                }
            }
            else
            {
                var response = _client.Search<T>(s => s.Size(ids.Count()).Query(q => q.Ids(c => c.Values(ids))));
                if (!response.IsValid)
                {
                    _logger.Error(InternalError);
                    throw new InvalidOperationException(InternalError);
                }
                if (response.Documents != null && response.Documents.Any())
                {
                    elements.AddRange(response.Documents);
                }
            }

            return elements.Any() ? elements
                                  : throw new EntityNotFoundException(string.Format(NotFoundByIdException, string.Join(", ", ids)));
        }

        /// <summary>
        /// Удалить сущность по идентификатору
        /// </summary>
        /// <typeparam name="T">Тип сущности</typeparam>
        /// <param name="id">Идетификатор сущности</param>
        public void Delete<T>(Guid id) where T : class
        {
            var response = _client.Delete(new DocumentPath<T>(new Id(id.ToString())));

            if (!response.IsValid)
            {
                _logger.Error(InternalError);
                throw new InvalidOperationException(InternalError);
            }
        }

        /// <summary>
        /// Удалить сущность по идентификатору
        /// </summary>
        /// <typeparam name="T">Тип сущности</typeparam>
        /// <param name="id">Идетификатор сущности</param>
        public async Task DeleteAsync<T>(Guid id) where T : class
        {
            var response = await _client.DeleteAsync(new DocumentPath<T>(new Id(id.ToString())));

            if (!response.IsValid)
            {
                _logger.Error(InternalError);
                throw new InvalidOperationException(InternalError);
            }
        }

        /// <summary>
        /// Получить указанное количество сущностей
        /// </summary>
        /// <typeparam name="T">Тип сущности</typeparam>
        /// <param name="maxRowCount">Максимальное количество строк</param>
        /// <returns></returns>
        public List<T> GetAll<T>(int maxRowCount) where T : class
        {
            var response = _client.Search<T>
                   (t => t
                         .Size(maxRowCount)
                         .MatchAll()
                   );

            if (!response.IsValid)
            {
                _logger.Error(InternalError);
                throw new InvalidOperationException(InternalError);
            }
            return response.Documents?.ToList() ?? new List<T>();
        }

        /// <summary>
        /// Получить указанное количество сущностей асинхронно
        /// </summary>
        /// <typeparam name="T">Тип сущности</typeparam>
        /// <param name="maxRowCount">Максимальное количество строк</param>
        /// <returns></returns>
        public async Task<List<T>> GetAllAsync<T>(int maxRowCount) where T : class
        {
            var response = await _client.SearchAsync<T>
                   (t => t
                         .Size(maxRowCount)
                         .MatchAll()
                   );

            if (!response.IsValid)
            {
                _logger.Error(InternalError);
                throw new InvalidOperationException(InternalError);
            }
            return response.Documents?.ToList() ?? new List<T>();
        }

        /// <summary>
        /// Получить все документы с использованием запроса
        /// </summary>
        /// <typeparam name="T">Тип сущности</typeparam>
        /// <param name="queryContainer">Поисковый запрос</param>
        /// <param name="sortField">Условие сортировки по полю</param>
        /// <param name="maxRowCount">Максимальное количество строк в ответе</param>
        /// <param name="from">Начальная позиция (по умолчанию 0)</param>
        /// <returns></returns>
        public List<T> Search<T>(QueryContainer queryContainer, FieldSortDescriptor<T> sortField, int maxRowCount, int from = 0) where T : class
        {
            var response = _client.Search<T>(t => t.From(from)
                                                   .Size(maxRowCount)
                                                   .Query(q => queryContainer)
                                                   .Sort(s => s.Field(f => sortField)));

            if (!response.IsValid)
            {
                _logger.Error(InternalError);
                throw new InvalidOperationException(InternalError);
            }
            return response.Documents?.ToList() ?? new List<T>();
        }

        /// <summary>
        /// Получить объект по условию
        /// </summary>
        /// <typeparam name="T">Тип сущности</typeparam>
        /// <param name="queryContainer">Поисковый запрос</param>
        /// <returns></returns>
        public async Task<T> GetByConditionAsync<T>(QueryContainer queryContainer) where T : class
        {
            var response = await _client.SearchAsync<T>(t => t.Size(1)
                                                              .Query(q => queryContainer));

            if (!response.IsValid)
            {
                _logger.Error(InternalError);
                throw new InvalidOperationException(InternalError);
            }
            return response.Documents != null && response.Documents.Any() ? response.Documents.First()
                                                                          : throw new EntityNotFoundException(NotFoundByConditionException);
        }

        /// <summary>
        /// Получить случайный документ по условию
        /// </summary>
        /// <typeparam name="T">Тип сущности</typeparam>
        /// <param name="queryContainer">Поисковый запрос</param>
        /// <returns></returns>
        public async Task<T> GetRandomByConditionAsync<T>(QueryContainer queryContainer) where T : class
        {
            var response = await _client.SearchAsync<T>(t => t
                                        .Size(1)
                                        .Query(q => q
                                        .FunctionScore(fs => fs.Query(fsq => queryContainer)
                                        .Functions(fu => fu
                                        .RandomScore()))));

            if (!response.IsValid)
            {
                _logger.Error(InternalError);
                throw new InvalidOperationException(InternalError);
            }
            return response.Documents != null && response.Documents.Any() ? response.Documents.First()
                                                                          : throw new EntityNotFoundException(NotFoundByConditionException);
        }


        /// <summary>
        /// Получить случайный набор документов по условию размером не более maxRowCount
        /// </summary>
        /// <typeparam name="T">Тип сущности</typeparam>
        /// <param name="queryContainer">Поисковый запрос</param>
        /// <param name="maxRowCount">Максимальное количество строк</param>
        /// <returns></returns>
        public async Task<List<T>> GetRandomListByConditionAsync<T>(QueryContainer queryContainer, int maxRowCount) where T : class
        {
            var response = await _client.SearchAsync<T>(t => t
                                        .Size(maxRowCount)
                                        .Query(q => q
                                        .FunctionScore(fs => fs.Query(fsq => queryContainer)
                                        .Functions(fu => fu
                                        .RandomScore()))));

            if (!response.IsValid)
            {
                _logger.Error(InternalError);
                throw new InvalidOperationException(InternalError);
            }
            return response.Documents?.ToList() ?? new List<T>();
        }

        /// <summary>
        /// Получить отсортированный список объектов по условию
        /// </summary>
        /// <typeparam name="T">Тип сущности</typeparam>
        /// <param name="queryContainer">Поисковый запрос</param>
        /// <param name="sortField">Условие сортировки по полю</param>
        /// <param name="maxRowCount">Максимальное количество строк в ответе</param>
        /// <param name="from">Начальная позиция (по умолчанию 0)</param>
        /// <returns></returns>
        public async Task<List<T>> SearchAsync<T>(QueryContainer queryContainer, FieldSortDescriptor<T> sortField, int maxRowCount, int from = 0) where T : class
        {
            var response = await _client.SearchAsync<T>(t => t.From(from)
                                                   .Size(maxRowCount)
                                                   .Query(q => queryContainer)
                                                   .Sort(s => s.Field(f => sortField)));

            if (!response.IsValid)
            {
                _logger.Error(InternalError);
                throw new InvalidOperationException(InternalError);
            }
            return response.Documents?.ToList() ?? new List<T>();
        }

        /// <summary>
        /// Получить количество записей по типу
        /// </summary>
        /// <typeparam name="T">Тип сущности</typeparam>
        /// <returns>количество</returns>
        public int Count<T>() where T : class
        {
            var response = _client.Count<T>(c => c.Query(q => q.MatchAll()));
            if (!response.IsValid)
            {
                _logger.Error(InternalError);
                throw new InvalidOperationException(InternalError);
            }
            return (int)response.Count;
        }

        /// <summary>
        /// Получить количество записей по типу с использование запроса
        /// </summary>
        /// <typeparam name="T">Тип сущности</typeparam>
        /// <param name="queryContainer">Поисковый запрос</param>
        /// <returns>количество</returns>
        public int Count<T>(QueryContainer queryContainer) where T : class
        {
            var response = _client.Count<T>(s => s.Query(q => queryContainer));
            if (!response.IsValid)
            {
                _logger.Error(InternalError);
                throw new InvalidOperationException(InternalError);
            }
            return (int)response.Count;
        }

        /// <summary>
        /// Получить количество записей по типу с использование запроса
        /// </summary>
        /// <typeparam name="T">Тип сущности</typeparam>
        /// <param name="queryContainer">Поисковый запрос</param>
        /// <returns>количество</returns>
        public async Task<int> CountAsync<T>(QueryContainer queryContainer) where T : class
        {
            var response = await _client.CountAsync<T>(s => s.Query(q => queryContainer));
            if (!response.IsValid)
            {
                _logger.Error(InternalError);
                throw new InvalidOperationException(InternalError);
            }
            return (int)response.Count;
        }

        /// <summary>
        /// Получить количество общее количество записей по типу
        /// </summary>
        /// <typeparam name="T">Тип сущности</typeparam>
        /// <returns>количество</returns>
        public async Task<int> CountAsync<T>() where T : class
        {
            var response = await _client.CountAsync<T>(c => c.Query(q => q.MatchAll()));
            if (!response.IsValid)
            {
                _logger.Error(InternalError);
                throw new InvalidOperationException(InternalError);
            }
            return (int)response.Count;
        }
    }
}
