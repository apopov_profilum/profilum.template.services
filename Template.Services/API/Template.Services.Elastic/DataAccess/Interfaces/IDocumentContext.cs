﻿using Nest;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Template.Services.Elastic.DataAccess.Interfaces
{
    /// <summary>
    /// Доступ к документо-ориентированной БД
    /// </summary>
    public interface IDocumentContext
    {
        /// <summary>
        /// Добавить документ
        /// </summary>
        /// <typeparam name="T">Тип сущности</typeparam>
        /// <param name="value">Добавляемое значение</param>
        void Add<T>(T value) where T : class;

        /// <summary>
        /// Добавить документ
        /// </summary>
        /// <typeparam name="T">Тип сущности</typeparam>
        /// <param name="value">Добавляемое значение</param>
        Task AddAsync<T>(T value) where T : class;

        /// <summary>
        /// Обновить документ
        /// </summary>
        /// <typeparam name="T">Тип сущности</typeparam>
        /// <param name="value">Добавляемое значение</param>
        void Update<T>(T value) where T : class;

        /// <summary>
        /// Обновить документ асинхронно
        /// </summary>
        /// <typeparam name="T">Тип сущности</typeparam>
        /// <param name="value">Добавляемое значение</param>
        Task UpdateAsync<T>(T value) where T : class;


        /// <summary>
        /// Получить данные по документу
        /// </summary>
        /// <typeparam name="T">Тип сущности</typeparam>
        /// <param name="id">Идетификатор документа</param>
        /// <returns></returns>
        T GetById<T>(Guid id) where T : class;

        /// <summary>
        /// </summary>
        /// <typeparam name="T">Тип сущности</typeparam>
        /// <param name="id">Идетификатор документа</param>
        /// <returns></returns>
        Task<T> GetByIdAsync<T>(Guid id) where T : class;

        /// <summary>
        /// Получить данные по нескольким документам
        /// </summary>
        /// <typeparam name="T">Тип сущности</typeparam>
        /// <param name="ids">Идетификаторы документов</param>
        /// <returns></returns>
        IEnumerable<T> GetByIds<T>(ICollection<Guid> ids) where T : class;

        /// <summary>
        /// Получить случайный документ по условию
        /// </summary>
        /// <typeparam name="T">Тип сущности</typeparam>
        /// <param name="queryContainer">Поисковый запрос</param>
        /// <returns></returns>
        Task<T> GetRandomByConditionAsync<T>(QueryContainer queryContainer) where T : class;

        /// <summary>
        /// Получить случайный набор документов по условию размером не более maxRowCount
        /// </summary>
        /// <typeparam name="T">Тип сущности</typeparam>
        /// <param name="queryContainer">Поисковый запрос</param>
        /// <param name="maxRowCount">Максимальное количество строк</param>
        /// <returns></returns>
        Task<List<T>> GetRandomListByConditionAsync<T>(QueryContainer queryContainer, int maxRowCount) where T : class;

        /// <summary>
        /// Удалить документ по идентификатору
        /// </summary>
        /// <typeparam name="T">Тип сущности</typeparam>
        /// <param name="id">Идетификатор сущности</param>
        void Delete<T>(Guid id) where T : class;

        /// <summary>
        /// Удалить документ по идентификатору
        /// </summary>
        /// <typeparam name="T">Тип сущности</typeparam>
        /// <param name="id">Идетификатор сущности</param>
        Task DeleteAsync<T>(Guid id) where T : class;

        /// <summary>
        /// Получить все записи
        /// </summary>
        /// <typeparam name="T">Тип сущности</typeparam>
        /// <param name="maxRowCount">Максимальное количество строк</param>
        /// <returns></returns>
        List<T> GetAll<T>(int maxRowCount) where T : class;

        /// <summary>
        /// Получить все записи асинхронно
        /// </summary>
        /// <typeparam name="T">Тип сущности</typeparam>
        /// <param name="maxRowCount">Максимальное количество строк</param>
        /// <returns></returns>
        Task<List<T>> GetAllAsync<T>(int maxRowCount) where T : class;

        /// <summary>
        /// Получить один документ с использованием запроса async
        /// </summary>
        /// <typeparam name="T">Тип сущности</typeparam>
        /// <param name="queryContainer">Поисковый запрос</param>
        /// <returns></returns>
        Task<T> GetByConditionAsync<T>(QueryContainer queryContainer) where T : class;

        /// <summary>
        /// Получить все документы с использованием запроса
        /// </summary>
        /// <typeparam name="T">Тип сущности</typeparam>
        /// <param name="queryContainer">Поисковый запрос</param>
        /// <param name="sortField">Условие сортировки по полю</param>
        /// <param name="maxRowCount">Максимальное количество строк в ответе</param>
        /// <param name="from">Начальная позиция (по умолчанию 0)</param>
        /// <returns></returns>
        List<T> Search<T>(QueryContainer queryContainer, FieldSortDescriptor<T> sortField, int maxRowCount, int from = 0) where T : class;

        /// <summary>
        /// Получить все документы с использованием запроса async
        /// </summary>
        /// <typeparam name="T">Тип сущности</typeparam>
        /// <param name="queryContainer">Поисковый запрос</param>
        /// <param name="sortField">Условие сортировки по полю</param>
        /// <param name="maxRowCount">Максимальное количество строк в ответе</param>
        /// <param name="from">Начальная позиция (по умолчанию 0)</param>
        /// <returns></returns>
        Task<List<T>> SearchAsync<T>(QueryContainer queryContainer, FieldSortDescriptor<T> sortField, int maxRowCount, int from = 0) where T : class;

        /// <summary>
        /// Получить количество записей по типу
        /// </summary>
        /// <typeparam name="T">Тип сущности</typeparam>
        /// <returns>количество</returns>
        int Count<T>() where T : class;

        /// <summary>
        /// Получить количество записей по типу с использование запроса
        /// </summary>
        /// <typeparam name="T">Тип сущности</typeparam>
        /// <param name="queryContainer">Поисковый запрос</param>
        /// <returns>количество</returns>
        int Count<T>(QueryContainer queryContainer) where T : class;

        /// <summary>
        /// Получить количество записей по типу с использование запроса async
        /// </summary>
        /// <typeparam name="T">Тип сущности</typeparam>
        /// <param name="queryContainer">Поисковый запрос</param>
        /// <returns>количество</returns>
        Task<int> CountAsync<T>(QueryContainer queryContainer) where T : class;

        /// <summary>
        /// Получить количество общее количество записей по типу async 
        /// </summary>
        /// <typeparam name="T">Тип сущности</typeparam>
        /// <returns>количество</returns>
        Task<int> CountAsync<T>() where T : class;
    }
}
