﻿using Microsoft.Extensions.Options;
using Nest;
using Template.Services.Elastic.Config;
using Template.Services.Elastic.DataAccess.Interfaces;
using Serilog;

namespace Template.Services.Elastic.DataAccess
{
    public class ElasticRecordContext : DocumentContext, IElasticRecordContext
    {
        private readonly IOptions<ElasticServiceSettings> _settings;

        public ElasticRecordContext(ILogger logger,
                                    IElasticClient client
                                   )
            : base(logger, client)
        {
        }
    }
}
