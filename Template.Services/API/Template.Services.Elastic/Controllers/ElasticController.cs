﻿using Microsoft.AspNetCore.Mvc;
using System.Net;
using System.Threading.Tasks;
using Template.Services.Dtos.Elastic.Requests;
using Template.Services.Dtos.Elastic.Responses;
using Template.Services.Elastic.Managers.Interfaces;

namespace Template.Services.Elastic.Controllers
{
    /// <summary>
    /// Chests controller
    /// </summary>
    [Route("api/v{version:apiVersion}/[controller]")]
    [Produces("application/json")]
    [ApiVersion("1.0")]
    public class ElasticController : ControllerBase
    {
        private readonly IElasticManager _elasticManager;

        public ElasticController(IElasticManager elasticManager)
        {
            _elasticManager = elasticManager;
        }

        /// <summary>
        /// Get one document by ID
        /// </summary>
        /// <param name="request"></param>
        /// <returns>Chest</returns>
        [HttpGet]
        [Route("getone")]
        [ProducesResponseType((int)HttpStatusCode.OK, Type = typeof(GetElasticDocumentResponse))]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        public async Task<GetElasticDocumentResponse> GetOne(GetElasticDocumentRequest request)
        {
            return await _elasticManager.GetOneAsync(request);
        }

        /// <summary>
        /// Get some documents
        /// </summary>
        /// <param name="request"></param>
        /// <returns>Chest</returns>
        [HttpGet]
        [Route("getsome")]
        [ProducesResponseType((int)HttpStatusCode.OK, Type = typeof(GetElasticDocumentResponse))]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        public async Task<GetElasticDocumentsResponse> GetSome(GetElasticDocumentsRequest request)
        {
            return await _elasticManager.GetSomeAsync(request);
        }

        /// <summary>
        /// Create document
        /// </summary>
        /// <param name="request"></param>
        /// <returns>Create Response</returns>
        [HttpPost]
        [Route("create")]
        [ProducesResponseType((int)HttpStatusCode.OK, Type = typeof(CreateElasticDocumentResponse))]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        public async Task<CreateElasticDocumentResponse> Create([FromBody] CreateElasticDocumentRequest request)
        {
            return await _elasticManager.CreateAsync(request);
        }

        /// <summary>
        /// Update document
        /// </summary>
        /// <param name="request"></param>
        /// <returns>Create Response</returns>
        [HttpPut]
        [Route("update")]
        [ProducesResponseType((int)HttpStatusCode.OK, Type = typeof(UpdateElasticDocumentResponse))]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        public async Task<UpdateElasticDocumentResponse> Update([FromBody] UpdateElasticDocumentRequest request)
        {
            return await _elasticManager.UpdateAsync(request);
        }

        /// <summary>
        /// Update document
        /// </summary>
        /// <param name="request"></param>
        /// <returns>Create Response</returns>
        [HttpDelete]
        [Route("delete")]
        [ProducesResponseType((int)HttpStatusCode.OK, Type = typeof(DeleteElasticDocumentResponse))]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        public async Task<DeleteElasticDocumentResponse> Delete([FromBody] DeleteElasticDocumentRequest request)
        {
            return await _elasticManager.DeleteAsync(request);
        }
    }
}
