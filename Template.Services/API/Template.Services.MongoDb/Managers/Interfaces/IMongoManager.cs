﻿using Template.Services.Dtos.MongoDb.Requests;
using Template.Services.Dtos.MongoDb.Responses;
using System.Threading.Tasks;

namespace Template.Services.MongoDb.Managers.Interfaces
{
    public interface IMongoManager
    {
        Task<CreateMongoDocumentResponse> CreateAsync(CreateMongoDocumentRequest request);

        Task<GetMongoDocumentResponse> GetOneAsync(GetMongoDocumentRequest request);

        Task<GetMongoDocumentsResponse> GetSomeAsync(GetMongoDocumentsRequest request);

        Task<UpdateMongoDocumentResponse> UpdateAsync(UpdateMongoDocumentRequest request);

        Task<DeleteMongoDocumentResponse> DeleteAsync(DeleteMongoDocumentRequest request);
    }
}
