﻿using AutoMapper;
using MongoDB.Driver;
using Template.Services.Dtos.MongoDb.Dto;
using Template.Services.Dtos.MongoDb.Requests;
using Template.Services.Dtos.MongoDb.Responses;
using Template.Services.MongoDb.DataAccess.Interfaces;
using Template.Services.MongoDb.Managers.Interfaces;
using Template.Services.MongoDb.Models;
using Serilog;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Template.Services.MongoDb.Managers
{
    public class MongoManager : IMongoManager
    {
        private readonly IMapper _mapper;
        private readonly ILogger _logger;

        private readonly IMongoContext _dbContext;

        public MongoManager(IMapper mapper,
                            ILogger logger,
                            IMongoContext dbContext)
        {
            _mapper = mapper;
            _logger = logger;
            _dbContext = dbContext;
        }

        public async Task<CreateMongoDocumentResponse> CreateAsync(CreateMongoDocumentRequest request)
        {
            try
            {
                await _dbContext.Documents.InsertOneAsync(_mapper.Map<MongoDocumentDto, MongoDocument>(request.Document));
            }
            catch (Exception ex)
            {
                _logger.Error(ex, $"Can't create MongoDocument with id = {request.Document.Id}");
                return new CreateMongoDocumentResponse
                {
                    Status = "Failed"
                };
            }
            

            return new CreateMongoDocumentResponse
            {
                Id = request.Document.Id,
                Status = "Success"
            };
        }

        public async Task<DeleteMongoDocumentResponse> DeleteAsync(DeleteMongoDocumentRequest request)
        {
            try
            {
                await _dbContext.Documents.DeleteOneAsync(d => d.Id == request.Id.ToString());
            }
            catch (Exception ex)
            {
                _logger.Error(ex, $"Can't delete MongoDocument with id = {request.Id}");
                return new DeleteMongoDocumentResponse
                {
                    Status = "Failed"
                };
            }

            return new DeleteMongoDocumentResponse
            {
                Status = "Success"
            };
        }

        public async Task<GetMongoDocumentResponse> GetOneAsync(GetMongoDocumentRequest request)
        {
            var document = (await _dbContext.Documents.FindAsync(d => d.Id == request.Id.ToString())).FirstOrDefault();

            if (document == null)
            {
                return new GetMongoDocumentResponse { Status = "Failed" };
            }

            return new GetMongoDocumentResponse
            {
                Document = _mapper.Map<MongoDocument, MongoDocumentDto>(document),
                Status = "Success"
            };
        }

        public async Task<GetMongoDocumentsResponse> GetSomeAsync(GetMongoDocumentsRequest request)
        {
            var documents = (await _dbContext.Documents.FindAsync(_ => true)).ToEnumerable();
            return new GetMongoDocumentsResponse
            {
                Documents = documents.Select(d => _mapper.Map<MongoDocument, MongoDocumentDto>(d)).ToList(),
                Status = "Success"
            };
        }

        public async Task<UpdateMongoDocumentResponse> UpdateAsync(UpdateMongoDocumentRequest request)
        {
            var result = await _dbContext.Documents.ReplaceOneAsync(d => d.Id == request.Document.Id.ToString(), _mapper.Map<MongoDocumentDto, MongoDocument>(request.Document));

            return new UpdateMongoDocumentResponse { Status = result.IsModifiedCountAvailable && result.ModifiedCount > 0 ? "Success" 
                                                                                                                          : "Failed" };
        }
    }
}
