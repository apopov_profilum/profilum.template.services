﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;
using AutoMapper;
using CorrelationId;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Template.Services.Core.Logging.SerilogEnrichers;
using Template.Services.Core.Swagger;
using Template.Services.Core.Swagger.DocumentFilters;
using Template.Services.Core.Swagger.OperationFilters;
using Template.Services.MongoDb.Config;
using Template.Services.MongoDb.Middleware;
using Serilog;
using Swashbuckle.AspNetCore.SwaggerGen;
using Template.Services.MongoDb.Infrastructure;

namespace Template.Services.MongoDb
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
#pragma warning restore CS1591 // Missing XML comment for publicly visible type or member
        {
            Configuration = configuration;
        }

#pragma warning disable CS1591 // Missing XML comment for publicly visible type or member
        public IConfiguration Configuration { get; }
#pragma warning restore CS1591 // Missing XML comment for publicly visible type or member

        // This method gets called by the runtime. Use this method to add services to the container.
#pragma warning disable CS1591 // Missing XML comment for publicly visible type or member
        public void ConfigureServices(IServiceCollection services)
#pragma warning restore CS1591 // Missing XML comment for publicly visible type or member
        {
            services.AddCorrelationId();

            services.AddMvc().SetCompatibilityVersion(CompatibilityVersion.Version_2_2);

            services.AddAutoMapper(Assembly.GetEntryAssembly());
            services.AddRouting(options => { options.LowercaseUrls = true; options.AppendTrailingSlash = false; });

            services.AddApiVersioning(o => o.ReportApiVersions = true);

            services.AddCors(options =>
            {
                options.AddPolicy("CorsPolicy",
                    builder => builder.AllowAnyOrigin()
                    .AllowAnyMethod()
                    .AllowAnyHeader()
                    .AllowCredentials());
            });

            // services.Configure<FormOptions>(options => options.BufferBody = true);
            services.Configure<MongoServiceSettings>(Configuration.GetSection(nameof(MongoServiceSettings)));

            var documentsConfigSection = Configuration.GetSection(nameof(MongoServiceSettings)).Get<MongoServiceSettings>();

            services.RegisterEasyNetQ(documentsConfigSection.RabbitMqConnection);           

            var logger = new LoggerConfiguration()
                .Enrich.With(new CorrelationIdEnricher(services.BuildServiceProvider().GetService<ICorrelationContextAccessor>()))
                .ReadFrom.Configuration(Configuration)
                .CreateLogger();
            services.AddSingleton<Serilog.ILogger>(logger);

            services.AddLogging(builder => builder.AddSerilog(logger));

            services.AddBusinessLogic();

            services.AddSwaggerGen(c =>
            {
                c.SwaggerDoc("v1.0", SwaggerApiInfoGetter.CreateInfoForApiVersion("v1.0", "MongoDB"));

                c.DocInclusionPredicate((docName, apiDesc) =>
                {
                    if (!apiDesc.TryGetMethodInfo(out MethodInfo methodInfo)) return false;

                    var versions = methodInfo.DeclaringType
                        .GetCustomAttributes(true)
                        .OfType<ApiVersionAttribute>()
                        .SelectMany(attr => attr.Versions);

                    return versions.Any(v => $"v{v.ToString()}" == docName);
                });

                c.OperationFilter<RemoveVersionOperationFilter>();
                c.OperationFilter<CorrelationFieldsOperationFilter>();

                //c.OperationFilter<FileUploadOperation>();

                c.DocumentFilter<SetVersionInPathDocumentFilter>();
                c.DocumentFilter<LowercaseDocumentFilter>();
                var path = Path.Combine($"{AppContext.BaseDirectory}", $"{Assembly.GetEntryAssembly().GetName().Name}.xml");
                c.IncludeXmlComments(Path.Combine($"{AppContext.BaseDirectory}", $"{Assembly.GetEntryAssembly().GetName().Name}.xml"));
                c.DescribeAllEnumsAsStrings();
            });
        }

#pragma warning disable CS1591 // Missing XML comment for publicly visible type or member
        public void Configure(IApplicationBuilder app, IHostingEnvironment env,
#pragma warning restore CS1591 // Missing XML comment for publicly visible type or member
                        IApplicationLifetime appLifetime)
        {
            app.UseCorrelationId(new CorrelationIdOptions
            {
                UseGuidForCorrelationId = true,
                UpdateTraceIdentifier = false
            });

            app.UseMvc();
            app.ConfigureEventBus();

            app.UseSwagger(c =>
            {
                c.RouteTemplate = "api/docs/{documentName}/swagger.json";
            });

            if (env.IsDevelopment())
            {
                app.UseSwaggerUI(c =>
                {
                    c.RoutePrefix = "api/docs";

                    c.SwaggerEndpoint("/api/docs/v1.0/swagger.json", "v1.0");
                });
            }
        }
    }
}
