﻿using AutoMapper;
using System;
using Template.Services.Dtos.MongoDb.Dto;
using Template.Services.MongoDb.Models;

namespace Template.Services.MongoDb.Mappings
{
    public class MappingProfile: Profile
    {
        public MappingProfile()
        {
            CreateMap<MongoDocument, MongoDocumentDto>()
                .ForMember(dto => dto.Id, model => model.MapFrom(v => Guid.Parse(v.Id)))
                .ReverseMap()
                .ForMember(model => model.Id, dto => dto.MapFrom(v => v.Id.ToString()));
        }
    }
}
