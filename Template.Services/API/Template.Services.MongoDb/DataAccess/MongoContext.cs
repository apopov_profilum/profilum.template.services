﻿using Microsoft.Extensions.Options;
using MongoDB.Bson;
using MongoDB.Driver;
using Template.Services.MongoDb.Config;
using Template.Services.MongoDb.DataAccess.Interfaces;
using Template.Services.MongoDb.Models;

namespace Template.Services.MongoDb.DataAccess
{
    /// <summary>
    /// MongoDB access
    /// </summary>
    public class MongoContext : IMongoContext
    {
        private readonly IMongoDatabase _db;

        public MongoContext(IOptions<MongoServiceSettings> settings)
        {
            // client to access with db
            var client = new MongoClient(settings.Value.MongoDBConnection);
            // db access
            _db = client.GetDatabase("Test");
        }

        public IMongoCollection<MongoDocument> Documents => _db.GetCollection<MongoDocument>("Documents");
    }
}
