﻿using MongoDB.Driver;
using Template.Services.MongoDb.Models;

namespace Template.Services.MongoDb.DataAccess.Interfaces
{
    public interface IMongoContext
    {
        IMongoCollection<MongoDocument> Documents { get; }
    }
}
