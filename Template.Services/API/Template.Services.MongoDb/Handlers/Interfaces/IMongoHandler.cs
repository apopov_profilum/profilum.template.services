﻿using Template.Services.Dtos.Elastic.Requests;
using Template.Services.Dtos.Elastic.Responses;
using System.Threading.Tasks;

namespace Template.Services.MongoDb.Handlers.Interfaces
{
    public interface IMongoHandler
    {
        Task<CreateElasticDocumentResponse> CreateAsync(CreateElasticDocumentRequest request);

        Task<GetElasticDocumentResponse> GetOneAsync(GetElasticDocumentRequest request);

        Task<GetElasticDocumentsResponse> GetSomeAsync(GetElasticDocumentsRequest request);

        Task<UpdateElasticDocumentResponse> UpdateAsync(UpdateElasticDocumentRequest request);

        Task<DeleteElasticDocumentResponse> DeleteAsync(DeleteElasticDocumentRequest request);
    }
}
