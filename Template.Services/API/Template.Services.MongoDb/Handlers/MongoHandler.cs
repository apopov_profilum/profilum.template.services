﻿using Template.Services.Dtos.Elastic.Requests;
using Template.Services.Dtos.Elastic.Responses;
using Template.Services.MongoDb.Handlers.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Template.Services.MongoDb.Handlers
{
    public class MongoHandler : IMongoHandler 
    {
        public MongoHandler()
        {

        }

        public Task<CreateElasticDocumentResponse> CreateAsync(CreateElasticDocumentRequest request)
        {
            throw new NotImplementedException();
        }

        public Task<DeleteElasticDocumentResponse> DeleteAsync(DeleteElasticDocumentRequest request)
        {
            throw new NotImplementedException();
        }

        public Task<GetElasticDocumentResponse> GetOneAsync(GetElasticDocumentRequest request)
        {
            throw new NotImplementedException();
        }

        public Task<GetElasticDocumentsResponse> GetSomeAsync(GetElasticDocumentsRequest request)
        {
            throw new NotImplementedException();
        }

        public Task<UpdateElasticDocumentResponse> UpdateAsync(UpdateElasticDocumentRequest request)
        {
            throw new NotImplementedException();
        }
    }
}
