﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using Template.Services.Dtos.MongoDb.Requests;
using Template.Services.Dtos.MongoDb.Responses;
using Template.Services.MongoDb.Managers.Interfaces;

namespace Template.Services.MongoDb.Controllers
{
    /// <summary>
    /// Chests controller
    /// </summary>
    [Route("api/v{version:apiVersion}/[controller]")]
    [Produces("application/json")]
    [ApiVersion("1.0")]
    public class MongoController : ControllerBase
    {
        private readonly IMongoManager _mongoManager;

        public MongoController(IMongoManager mongoManager)
        {
            _mongoManager = mongoManager;
        }

        /// <summary>
        /// Get one document by ID
        /// </summary>
        /// <param name="request"></param>
        /// <returns>Chest</returns>
        [HttpGet]
        [Route("getone")]
        [ProducesResponseType((int)HttpStatusCode.OK, Type = typeof(GetMongoDocumentResponse))]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        public async Task<GetMongoDocumentResponse> GetOne(GetMongoDocumentRequest request)
        {
            return await _mongoManager.GetOneAsync(request);
        }

        /// <summary>
        /// Get some documents
        /// </summary>
        /// <param name="request"></param>
        /// <returns>Chest</returns>
        [HttpGet]
        [Route("getsome")]
        [ProducesResponseType((int)HttpStatusCode.OK, Type = typeof(GetMongoDocumentResponse))]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        public async Task<GetMongoDocumentsResponse> GetSome(GetMongoDocumentsRequest request)
        {
            return await _mongoManager.GetSomeAsync(request);
        }

        /// <summary>
        /// Create document
        /// </summary>
        /// <param name="request"></param>
        /// <returns>Create Response</returns>
        [HttpPost]
        [Route("create")]
        [ProducesResponseType((int)HttpStatusCode.OK, Type = typeof(CreateMongoDocumentResponse))]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        public async Task<CreateMongoDocumentResponse> Create([FromBody] CreateMongoDocumentRequest request)
        {
            return await _mongoManager.CreateAsync(request);
        }

        /// <summary>
        /// Update document
        /// </summary>
        /// <param name="request"></param>
        /// <returns>Create Response</returns>
        [HttpPut]
        [Route("update")]
        [ProducesResponseType((int)HttpStatusCode.OK, Type = typeof(UpdateMongoDocumentResponse))]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        public async Task<UpdateMongoDocumentResponse> Update([FromBody] UpdateMongoDocumentRequest request)
        {
            return await _mongoManager.UpdateAsync(request);
        }

        /// <summary>
        /// Update document
        /// </summary>
        /// <param name="request"></param>
        /// <returns>Create Response</returns>
        [HttpDelete]
        [Route("delete")]
        [ProducesResponseType((int)HttpStatusCode.OK, Type = typeof(DeleteMongoDocumentResponse))]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        public async Task<DeleteMongoDocumentResponse> Delete([FromBody] DeleteMongoDocumentRequest request)
        {
            return await _mongoManager.DeleteAsync(request);
        }
    }
}
