﻿namespace Template.Services.MongoDb.Config
{
    public class MongoServiceSettings
    {
        /// <summary>
        /// RabbitMq connection string
        /// </summary>
        public string RabbitMqConnection { get; set; }

        /// <summary>
        /// MongoDB connection string
        /// </summary>
        public string MongoDBConnection { get; set; }
    }
}
