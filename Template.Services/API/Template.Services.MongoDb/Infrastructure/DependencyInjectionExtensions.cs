﻿using Microsoft.Extensions.DependencyInjection;
using Template.Services.MongoDb.DataAccess;
using Template.Services.MongoDb.DataAccess.Interfaces;
using Template.Services.MongoDb.Handlers;
using Template.Services.MongoDb.Handlers.Interfaces;
using Template.Services.MongoDb.Managers;
using Template.Services.MongoDb.Managers.Interfaces;

namespace Template.Services.MongoDb.Infrastructure
{
    public static class DependencyInjectionExtentions
    {
        public static void AddBusinessLogic(this IServiceCollection services)
        {
            services.AddTransient<IMongoContext, MongoContext>();
            services.AddTransient<IMongoManager, MongoManager>();
            services.AddTransient<IMongoHandler, MongoHandler>();
        }
    }
}
