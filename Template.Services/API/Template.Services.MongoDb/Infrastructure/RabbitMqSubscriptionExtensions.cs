﻿using Microsoft.AspNetCore.Builder;
using Microsoft.Extensions.DependencyInjection;
using Template.Services.Dtos.MongoDb.Requests;
using Template.Services.MongoDb.Managers.Interfaces;

namespace Template.Services.MongoDb.Infrastructure
{
    public static class RabbitMqSubscriptionExtensions
    {
        public static void ConfigureEventBus(this IApplicationBuilder builder)
        {
            var eventBus = builder.ApplicationServices.GetRequiredService<EasyNetQ.IBus>();

            eventBus.RespondAsync(async (CreateMongoDocumentRequest request) => await builder.ApplicationServices.CreateScope().ServiceProvider.GetService<IMongoManager>().CreateAsync(request));
            eventBus.RespondAsync(async (GetMongoDocumentRequest request) => await builder.ApplicationServices.CreateScope().ServiceProvider.GetService<IMongoManager>().GetOneAsync(request));
            eventBus.RespondAsync(async (GetMongoDocumentsRequest request) => await builder.ApplicationServices.CreateScope().ServiceProvider.GetService<IMongoManager>().GetSomeAsync(request));
            eventBus.RespondAsync(async (UpdateMongoDocumentRequest request) => await builder.ApplicationServices.CreateScope().ServiceProvider.GetService<IMongoManager>().UpdateAsync(request));
            eventBus.RespondAsync(async (DeleteMongoDocumentRequest request) => await builder.ApplicationServices.CreateScope().ServiceProvider.GetService<IMongoManager>().DeleteAsync(request));
        }
    }
}
