﻿# Описание AUTH сервиса:

Данный сервис используется в качестве точки входа для общения с frontend.
В дальнейшем Gateway сервис общается с другими микросервисами для совершения запрашиваемого действия.

Для общения с другими сервисами существует 2 типа коммуникаций:

1. Через брокер очередей **RabbitMQ** с использованием паттерна **Request-Respond** - базовый способ коммуникации. 
Для добавления нового метода нужно:
- Отправить запрос от **Gateway** с указанием типов **Request** и **Respond**.
```
public async Task<TestResponse> SendTestAsync(TestRequest request)
{
	return await _bus.RequestAsync<TestRequest, TestResponse>(request);
}
```

- В сервисе Consumer нужно открыть канал для прослушивания сообщений заданного типа в **Startup.cs** и указать обработчик (handler).
```
var eventBus = builder.ApplicationServices.GetRequiredService<EasyNetQ.IBus>();
var testHandler = builder.ApplicationServices.GetRequiredService<ITestHandler>();

eventBus.RespondAsync(async (TestRequest request) => await testHandler.TestHandlerAsync(request));
```

2. Через Http-запросы - используется только для вызова методов IdentityServer4. Все методы содержатся в классе **TokenClient**.