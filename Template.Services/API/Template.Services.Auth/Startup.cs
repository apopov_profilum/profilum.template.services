﻿using AutoMapper;
using CorrelationId;
using Serilog;
using System;
using System.IO;
using System.Linq;
using System.Reflection;
using Swashbuckle.AspNetCore.SwaggerGen;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Template.Services.Auth.Config;
using IdentityConfig = Template.Services.Auth.Config.Config;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Swashbuckle.AspNetCore.Swagger;
using Template.Services.Auth.Models;
using Template.Services.Auth.Managers.Interfaces;
using Template.Services.Auth.Handlers.Interfaces;
using Template.Services.Auth.Managers;
using Template.Services.Auth.Handlers;
using Template.Services.Core.Logging.SerilogEnrichers;
using Template.Services.Core.Swagger.OperationFilters;
using Template.Services.Core.Swagger.DocumentFilters;
using Template.Services.Core.Swagger;
using Template.Services.Auth.IdentityCustomization;
using Template.Services.Auth.Middleware;
using Microsoft.EntityFrameworkCore;
using Template.Services.Auth.Infrastructure;

namespace Template.Services.Auth
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddCorrelationId();

            services.AddDbContext<AuthContext>(options => options.UseNpgsql(Configuration.GetConnectionString("DefaultConnection")),
                ServiceLifetime.Transient);

            services.AddIdentity<User, IdentityRole<Guid>>(opts =>
            {
                opts.Password.RequiredLength = 6;
                opts.Password.RequireNonAlphanumeric = false;
                opts.Password.RequireDigit = true;
                opts.Password.RequireUppercase = false;
            })
                .AddEntityFrameworkStores<AuthContext>()
                .AddDefaultTokenProviders();

            services.AddTransient<UserStore>();
            services.AddTransient<UserManager<User>>();

            services.AddMvc().SetCompatibilityVersion(CompatibilityVersion.Version_2_2);

            services.AddAutoMapper(Assembly.GetEntryAssembly());
            services.AddRouting(options => { options.LowercaseUrls = true; options.AppendTrailingSlash = false; });

            services.AddApiVersioning(o => o.ReportApiVersions = true);

            var authConfigSection = Configuration.GetSection(nameof(AuthServiceSettings)).Get<AuthServiceSettings>();
            services.AddDistributedRedisCache(option =>
            {
                option.Configuration = authConfigSection.RedisCacheServiceConnection;
            });

            services.RegisterEasyNetQ(authConfigSection.RabbitMqConnection);

            services.AddSingleton<IHttpContextAccessor, HttpContextAccessor>();

            services.AddTransient<IAccountManager, AccountManager>();
            services.AddTransient<IAccountHandler, AccountHandler>();

            var clientsConfigSection = Configuration.GetSection("IdentityServer").Get<IdentityServerSettings>();

            services.AddIdentityServer()
                .AddDeveloperSigningCredential() // ONLY FOR DEVELOPMENT ENVIRONMENT!!!
                                                 //.AddSigningCredential(new Microsoft.IdentityModel.Tokens.RsaSecurityKey() //should be used instead addDeveloperSigningCredential
                .AddOperationalStore(options =>
                {
                    options.RedisConnectionString = authConfigSection.RedisCacheServiceConnection;
                    options.Db = authConfigSection.RedisDbNumber;
                })
                .AddRedisCaching(options =>
                {
                    options.RedisConnectionString = authConfigSection.RedisCacheServiceConnection;
                    options.KeyPrefix = "auth";
                })
                //.AddInMemoryPersistedGrants() // store IdentityServer keys in memory
                .AddInMemoryIdentityResources(IdentityConfig.GetIdentityResources())
                .AddInMemoryApiResources(IdentityConfig.GetApiResources())
                .AddInMemoryClients(clientsConfigSection.Clients)
                .AddAspNetIdentity<User>()
                .AddProfileService<IdentityClaimsProfileService>();

            var logger = new LoggerConfiguration()
                             .Enrich.With(new CorrelationIdEnricher(services.BuildServiceProvider().GetService<ICorrelationContextAccessor>()))
                             .ReadFrom.Configuration(Configuration)
                             .CreateLogger();
            services.AddSingleton<ILogger>(logger);

            services.AddLogging(builder => builder.AddSerilog(logger));

            services.AddSwaggerGen(c =>
            {
                c.SwaggerDoc("v1.0", SwaggerApiInfoGetter.CreateInfoForApiVersion("v1.0", "Auth"));

                c.DocInclusionPredicate((docName, apiDesc) =>
                {
                    if (!apiDesc.TryGetMethodInfo(out MethodInfo methodInfo)) return false;

                    var versions = methodInfo.DeclaringType
                        .GetCustomAttributes(true)
                        .OfType<ApiVersionAttribute>()
                        .SelectMany(attr => attr.Versions);

                    return versions.Any(v => $"v{v.ToString()}" == docName);
                });

                c.AddSecurityDefinition("basic", new BasicAuthScheme
                {
                    Type = "basic",
                    Description = "Basic Authentication"
                });

                c.OperationFilter<RemoveVersionOperationFilter>();
                c.OperationFilter<CorrelationFieldsOperationFilter>();

                c.DocumentFilter<SetVersionInPathDocumentFilter>();
                c.DocumentFilter<LowercaseDocumentFilter>();
                c.DocumentFilter<AuthTokenDocumentFilter>();

                c.IncludeXmlComments(Path.Combine($"{AppContext.BaseDirectory}", $"{Assembly.GetEntryAssembly().GetName().Name}.xml"));
                c.DescribeAllEnumsAsStrings();
            });
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env, IApplicationLifetime appLifetime)
        {
            app.UseCorrelationId(new CorrelationIdOptions
            {
                UseGuidForCorrelationId = true,
                UpdateTraceIdentifier = false
            });

            appLifetime.ApplicationStopped.Register(Log.CloseAndFlush);

            app.ConfigureEventBus();

            app.UseErrorLoggingMiddleware();

            app.UseIdentityServer();
            app.UseMvc();

            app.UseSwagger(c =>
            {
                c.RouteTemplate = "api/docs/{documentName}/swagger.json";
            });

            if (env.IsDevelopment())
            {
                app.UseSwaggerUI(c =>
                {
                    c.RoutePrefix = "api/docs";

                    c.SwaggerEndpoint("/api/docs/v1.0/swagger.json", "v1.0");
                });
            }
        }
    }
}
