﻿using Microsoft.AspNetCore.Mvc;
using Template.Services.Auth.Managers.Interfaces;
using Template.Services.Core.Constants;
using Template.Services.Dtos.Auth.Requests;
using Template.Services.Dtos.Auth.Responses;
using System.Net;
using System.Threading.Tasks;

namespace Template.Services.Auth.Controllers
{
    [Route("api/v{version:apiVersion}/[controller]")]
    [ApiVersion("1.0")]
    [ApiController]
    public class AccountController : Controller
    {
        private readonly IAccountManager _accountManager;

        public AccountController(IAccountManager accountManager)
        {
            _accountManager = accountManager;
        }

        /// <summary>
        /// Register 
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("register")]
        [ProducesResponseType((int)HttpStatusCode.OK, Type = typeof(RegisterUserResponse))]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        public async Task<IActionResult> Register([FromBody]RegisterUserRequest request)
        {
            var response = await _accountManager.RegisterUserAsync(request);
            if (response.Status == ResponseStatus.Success)
            {
                return Ok(response);
            }
            else
            {
                return BadRequest(response);
            }
        }

        /// <summary>
        /// Change password
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("changepassword")]
        [ProducesResponseType((int)HttpStatusCode.OK, Type = typeof(ChangePasswordResponse))]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        public async Task<IActionResult> ChangePassword([FromBody]ChangePasswordRequest request)
        {
            var response = await _accountManager.ChangePasswordAsync(request);
            if (response.Status == ResponseStatus.Success)
            {
                return Ok(response);
            }
            else
            {
                return BadRequest(response);
            }
        }

        /// <summary>
        /// Reset password
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("resetpassword")]
        [ProducesResponseType((int)HttpStatusCode.OK, Type = typeof(ResetPasswordResponse))]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        public async Task<IActionResult> ResetPassword([FromBody]ResetPasswordRequest request)
        {
            var response = await _accountManager.ResetPasswordAsync(request);
            if (response.Status == ResponseStatus.Success)
            {
                return Ok(response);
            }
            else
            {
                return BadRequest(response);
            }
        }
    }
}
