﻿using Microsoft.AspNetCore.Builder;
using Microsoft.Extensions.DependencyInjection;
using Template.Services.Dtos.Auth.Requests;
using Template.Services.Auth.Managers.Interfaces;

namespace Template.Services.Auth.Infrastructure
{
    public static class RabbitMqSubscriptionExtensions
    {
        public static void ConfigureEventBus(this IApplicationBuilder builder)
        {
            var eventBus = builder.ApplicationServices.GetRequiredService<EasyNetQ.IBus>();

            eventBus.RespondAsync(async (RegisterUserRequest request) => await builder.ApplicationServices.CreateScope().ServiceProvider.GetService<IAccountManager>().RegisterUserAsync(request));
            eventBus.RespondAsync(async (ChangePasswordRequest request) => await builder.ApplicationServices.CreateScope().ServiceProvider.GetService<IAccountManager>().ChangePasswordAsync(request));
            eventBus.RespondAsync(async (GetPasswordResetTokenRequest request) => await builder.ApplicationServices.CreateScope().ServiceProvider.GetService<IAccountManager>().GetPasswordResetTokenAsync(request));
            eventBus.RespondAsync(async (ResetPasswordRequest request) => await builder.ApplicationServices.CreateScope().ServiceProvider.GetService<IAccountManager>().ResetPasswordAsync(request));
        }
    }
}
