﻿using IdentityServer4.Models;
using System.Collections.Generic;

namespace Template.Services.Auth.Config
{
    public class IdentityServerSettings
    {
        public IEnumerable<Client> Clients { get; set; }
    }
}
