﻿namespace Template.Services.Auth.Config
{
    public class AuthServiceSettings
    {
        /// <summary>
        /// Ссылка на сервис кеширования
        /// </summary>
        public string RedisCacheServiceConnection { get; set; }

        /// <summary>
        /// Номер используемой БД в Redis
        /// </summary>
        public byte RedisDbNumber { get; set; }

        /// <summary>
        /// Ссылка на менеджер очередей RabbitMQ
        /// </summary>
        public string RabbitMqConnection { get; set; }
    }
}
