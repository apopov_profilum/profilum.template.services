﻿using IdentityModel;
using IdentityServer4;
using IdentityServer4.Models;
using System.Collections.Generic;

namespace Template.Services.Auth.Config
{
    public class Config
    {
        private static readonly IEnumerable<string> TemplateApiClaimTypes = new List<string> { JwtClaimTypes.Name, JwtClaimTypes.Email, JwtClaimTypes.Role };

        public static IEnumerable<IdentityResource> GetIdentityResources()
        {
            return new List<IdentityResource>
            {
            };
        }

        public static IEnumerable<ApiResource> GetApiResources()
        {
            return new List<ApiResource>
            {
                new ApiResource("template_api", "Template API", TemplateApiClaimTypes)
            };
        }
    }
}
