﻿using Template.Services.Dtos.Auth.Requests;
using Template.Services.Dtos.Auth.Responses;
using System.Threading.Tasks;

namespace Template.Services.Auth.Managers.Interfaces
{
    public interface IAccountManager
    {
        /// <summary>
        /// Register new user
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        Task<RegisterUserResponse> RegisterUserAsync(RegisterUserRequest request);

        /// <summary>
        /// Change password
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        Task<ChangePasswordResponse> ChangePasswordAsync(ChangePasswordRequest request);

        /// <summary>
        /// Reset password
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        Task<ResetPasswordResponse> ResetPasswordAsync(ResetPasswordRequest request);

        /// <summary>
        /// Get password reset token - TEST METHOD
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        Task<GetPasswordResetTokenResponse> GetPasswordResetTokenAsync(GetPasswordResetTokenRequest request);
    }
}
