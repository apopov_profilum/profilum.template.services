﻿using AutoMapper;
using IdentityModel;
using IdentityServer4.Stores;
using Microsoft.AspNetCore.Identity;
using Template.Services.Auth.Managers.Interfaces;
using Template.Services.Auth.Models;
using Template.Services.Core.Constants;
using Template.Services.Core.Logging.Extensions;
using Template.Services.Dtos.Auth.Requests;
using Template.Services.Dtos.Auth.Responses;
using Serilog;
using System;
using System.Threading.Tasks;

namespace Template.Services.Auth.Managers
{
    public class AccountManager: IAccountManager
    {
        private readonly UserManager<User> _userManager;
        private readonly RoleManager<IdentityRole<Guid>> _roleManager;
        private readonly IPersistedGrantStore _persistendGrantStore;
        private readonly IMapper _mapper;
        private readonly ILogger _logger;

        public AccountManager(UserManager<User> userManager,
                              RoleManager<IdentityRole<Guid>> roleManager,
                              IPersistedGrantStore persistedGrantStore,
                              ILogger logger,
                              IMapper mapper)
        {
            _userManager = userManager;
            _roleManager = roleManager;
            _persistendGrantStore = persistedGrantStore;
            _logger = logger;
            _mapper = mapper;
        }

        public async Task<RegisterUserResponse> RegisterUserAsync(RegisterUserRequest request)
        {
            _logger.Information(request.CorrelationId, request.CorrelationUserId, "Register new user");

            var user = new User
            {
                UserName = request.UserName,
                Email = request.Email
            };

            if (request.Password != request.ConfirmPassword) //TODO вынести эти проверки на уровень атрибутов модели
            {
                return new RegisterUserResponse
                {
                    Status = ResponseStatus.Failed,
                    Comment = "Password not equals ConfirmPassword"
                };
            }

            //check role
            var isRoleExists = await _roleManager.FindByNameAsync(request.Role) != null;

            if (!isRoleExists)
            {
                //await _roleManager.CreateAsync(new IdentityRole<Guid>(request.Role));
                return new RegisterUserResponse
                {
                    Status = ResponseStatus.Failed,
                    Comment = "Wrong role"
                };
            }

            var createResult = await _userManager.CreateAsync(user, request.Password);

            if (createResult.Succeeded)
            {
                await _userManager.AddToRoleAsync(user, request.Role);

                var response = new RegisterUserResponse
                {
                    Status = ResponseStatus.Success,
                    Comment = "User registered"
                };

                return response;
            }
            else
            {
                return new RegisterUserResponse
                {
                    Status = ResponseStatus.Failed,
                    Comment = createResult.ToString()
                };
            }
        }

        public async Task<ChangePasswordResponse> ChangePasswordAsync(ChangePasswordRequest request)
        {
            _logger.Information(request.CorrelationId, request.CorrelationUserId, "Change password");

            if (string.IsNullOrEmpty(request.CurrentPassword))
            {
                return new ChangePasswordResponse
                {
                    Status = ResponseStatus.Failed,
                    Comment = "Current password is empty"
                };
            }

            if (string.IsNullOrEmpty(request.NewPassword))
            {
                return new ChangePasswordResponse
                {
                    Status = ResponseStatus.Failed,
                    Comment = "New password is empty"
                };
            }

            var user = await _userManager.FindByIdAsync(request.UserId.ToString());
            if (user == null)
            {
                return new ChangePasswordResponse
                {
                    Status = ResponseStatus.Failed,
                    Comment = "Can't find user"
                };
            }

            var changePasswordResult = await _userManager.ChangePasswordAsync(user, request.CurrentPassword, request.NewPassword);
            if (!changePasswordResult.Succeeded)
            {
                return new ChangePasswordResponse
                {
                    Status = ResponseStatus.Failed,
                    Comment = "Change password error"
                };
            }

            // delete all refresh tokens for current user
            await _persistendGrantStore.RemoveAllAsync(request.UserId.ToString(), request.ClientId, OidcConstants.TokenTypes.RefreshToken);

            return new ChangePasswordResponse
            {
                Status = ResponseStatus.Success,
                Comment = "Password changed"
            };
        }

        public async Task<ResetPasswordResponse> ResetPasswordAsync(ResetPasswordRequest request)
        {
            _logger.Information(request.CorrelationId, request.CorrelationUserId, "Reset password");
            var user = await _userManager.FindByNameAsync(request.Email);
            if (user != null)
            {
                var result = await _userManager.ResetPasswordAsync(user, request.PasswordResetToken, request.Password);
                if (result.Succeeded)
                {
                    return new ResetPasswordResponse
                    {
                        Status = ResponseStatus.Success,
                        Comment = "Password reset"
                    };
                }
                return new ResetPasswordResponse
                {
                    Status = ResponseStatus.Failed,
                    Comment = result.ToString()

                };
            }
            return new ResetPasswordResponse
            {
                Status = ResponseStatus.Failed,
                Comment = "Invalid userName"

            };
        }

        public async Task<GetPasswordResetTokenResponse> GetPasswordResetTokenAsync(GetPasswordResetTokenRequest request)
        {
            _logger.Information(request.CorrelationId, request.CorrelationUserId, "Get password reset token");
            var user = await _userManager.FindByNameAsync(request.Email);
            if (user != null)
            {
                return new GetPasswordResetTokenResponse
                {
                    PasswordResetToken = await _userManager.GeneratePasswordResetTokenAsync(user),
                    Status = ResponseStatus.Success
                };
            }
            return new GetPasswordResetTokenResponse
            {
                Status = ResponseStatus.Failed
            };
        }
    }
}
