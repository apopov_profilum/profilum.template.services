﻿using Microsoft.AspNetCore.Identity;
using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace Template.Services.Auth.Models
{
    /// <summary>
    /// Marathon user
    /// </summary>
    public class User : IdentityUser<Guid>
    {
        /// <summary>
        /// Дата заведения пользователя в систему
        /// </summary>
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public DateTime CreatedDate { get; set; } = DateTime.Now;

        /// <summary>
        /// Признак наличия профиля у пользователя
        /// </summary>
        public bool ProfileExists { get; set; }
    }
}
