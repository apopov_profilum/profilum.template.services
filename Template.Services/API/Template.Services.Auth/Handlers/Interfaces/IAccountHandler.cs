﻿using Template.Services.Dtos.Auth.Requests;
using Template.Services.Dtos.Auth.Responses;
using System.Threading.Tasks;

namespace Template.Services.Auth.Handlers.Interfaces
{
    public interface IAccountHandler
    {
        Task<RegisterUserResponse> RegisterUserAsync(RegisterUserRequest request);

        Task<ChangePasswordResponse> ChangePasswordAsync(ChangePasswordRequest request);

        Task<GetPasswordResetTokenResponse> GetPasswordResetTokenAsync(GetPasswordResetTokenRequest request);

        Task<ResetPasswordResponse> ResetPasswordAsync(ResetPasswordRequest request);
    }
}
