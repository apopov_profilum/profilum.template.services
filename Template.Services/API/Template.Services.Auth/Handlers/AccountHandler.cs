﻿using Template.Services.Auth.Handlers.Interfaces;
using Template.Services.Auth.Managers.Interfaces;
using Template.Services.Core.Constants;
using Template.Services.Dtos.Auth.Requests;
using Template.Services.Dtos.Auth.Responses;
using Template.Services.Dtos.Base.Requests;
using Template.Services.Dtos.Base.Responses;
using System;
using System.Threading.Tasks;

namespace Template.Services.Auth.Handlers
{
    public class AccountHandler : IAccountHandler
    {
        private readonly IAccountManager _accountManager;

        public AccountHandler(IAccountManager accountManager)
        {
            _accountManager = accountManager;
        }

        public async Task<RegisterUserResponse> RegisterUserAsync(RegisterUserRequest request)
        {
            return await HandleActionAsync(request, (r) => _accountManager.RegisterUserAsync(request));
        }

        public async Task<ChangePasswordResponse> ChangePasswordAsync(ChangePasswordRequest request)
        {
            return await HandleActionAsync(request, (r) => _accountManager.ChangePasswordAsync(request));
        }

        public async Task<GetPasswordResetTokenResponse> GetPasswordResetTokenAsync(GetPasswordResetTokenRequest request)
        {
            return await HandleActionAsync(request, (r) => _accountManager.GetPasswordResetTokenAsync(request));
        }

        public async Task<ResetPasswordResponse> ResetPasswordAsync(ResetPasswordRequest request)
        {
            return await HandleActionAsync(request, (r) => _accountManager.ResetPasswordAsync(request));
        }

        /// <summary>
        /// Base method to handle action
        /// </summary>
        /// <typeparam name="TRequest">Request type</typeparam>
        /// <typeparam name="TResponse">Response type</typeparam>
        /// <param name="request"></param>
        /// <param name="action"></param>
        /// <returns></returns>
        private async Task<TResponse> HandleActionAsync<TRequest, TResponse>(TRequest request, Func<TRequest, Task<TResponse>> action) where TRequest : BaseRequest, new()
                                                                                                                                       where TResponse : BaseResponse, new()
        {
            try
            {
                return await action(request);
            }
            catch (Exception ex)
            {
                return new TResponse()
                {
                    Comment = ex.Message,
                    Status = ResponseStatus.Failed
                };
            }
        }
    }
}
