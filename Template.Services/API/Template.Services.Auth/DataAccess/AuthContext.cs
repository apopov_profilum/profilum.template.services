﻿using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using Template.Services.Auth.Models;
using System;
using System.Threading.Tasks;

namespace Template.Services.Auth
{
    public partial class AuthContext : IdentityDbContext<User, IdentityRole<Guid>, Guid>
    {
        public AuthContext(DbContextOptions<AuthContext> options)
            : base(options)
        {
        }

        public Task<int> SaveChangesAsync() => base.SaveChangesAsync();

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);

            modelBuilder.Entity<User>().ToTable("Users");
            modelBuilder.Entity<IdentityUserClaim<Guid>>().ToTable("UserClaims");
            modelBuilder.Entity<IdentityUserRole<Guid>>().ToTable("UserRoles");

            modelBuilder.Entity<IdentityRole<Guid>>().ToTable("Roles");
            modelBuilder.Entity<IdentityRoleClaim<Guid>>().ToTable("RoleClaims");

            modelBuilder.Entity<IdentityUserToken<Guid>>().ToTable("Tokens");
            modelBuilder.Entity<IdentityUserLogin<Guid>>().ToTable("Logins");
        }
    }
}
