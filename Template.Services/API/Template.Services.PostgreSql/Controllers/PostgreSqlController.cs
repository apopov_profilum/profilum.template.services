﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using Template.Services.Dtos.PostgreSql.Requests;
using Template.Services.Dtos.PostgreSql.Responses;
using Template.Services.PostgreSql.Managers.Interfaces;

namespace Template.Services.PostgreSql.Controllers
{
    /// <summary>
    /// Chests controller
    /// </summary>
    [Route("api/v{version:apiVersion}/[controller]")]
    [Produces("application/json")]
    [ApiVersion("1.0")]
    public class ElasticController : ControllerBase
    {
        private readonly IPostgreSqlManager _postgreManager;

        public ElasticController(IPostgreSqlManager postgreManager)
        {
            _postgreManager = postgreManager;
        }

        /// <summary>
        /// Get one document by ID
        /// </summary>
        /// <param name="request"></param>
        /// <returns>Chest</returns>
        [HttpGet]
        [Route("getone")]
        [ProducesResponseType((int)HttpStatusCode.OK, Type = typeof(GetDbRecordResponse))]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        public async Task<GetDbRecordResponse> GetOne(GetDbRecordRequest request)
        {
            return await _postgreManager.GetOneAsync(request);
        }

        /// <summary>
        /// Get some documents
        /// </summary>
        /// <param name="request"></param>
        /// <returns>Chest</returns>
        [HttpGet]
        [Route("getsome")]
        [ProducesResponseType((int)HttpStatusCode.OK, Type = typeof(GetDbRecordResponse))]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        public async Task<GetDbRecordsResponse> GetSome(GetDbRecordsRequest request)
        {
            return await _postgreManager.GetSomeAsync(request);
        }

        /// <summary>
        /// Create document
        /// </summary>
        /// <param name="request"></param>
        /// <returns>Create Response</returns>
        [HttpPost]
        [Route("create")]
        [ProducesResponseType((int)HttpStatusCode.OK, Type = typeof(CreateDbRecordResponse))]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        public async Task<CreateDbRecordResponse> Create([FromBody] CreateDbRecordRequest request)
        {
            return await _postgreManager.CreateAsync(request);
        }

        /// <summary>
        /// Update document
        /// </summary>
        /// <param name="request"></param>
        /// <returns>Create Response</returns>
        [HttpPut]
        [Route("update")]
        [ProducesResponseType((int)HttpStatusCode.OK, Type = typeof(UpdateDbRecordResponse))]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        public async Task<UpdateDbRecordResponse> Update([FromBody] UpdateDbRecordRequest request)
        {
            return await _postgreManager.UpdateAsync(request);
        }

        /// <summary>
        /// Update document
        /// </summary>
        /// <param name="request"></param>
        /// <returns>Create Response</returns>
        [HttpDelete]
        [Route("delete")]
        [ProducesResponseType((int)HttpStatusCode.OK, Type = typeof(DeleteDbRecordResponse))]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        public async Task<DeleteDbRecordResponse> Delete([FromBody] DeleteDbRecordRequest request)
        {
            return await _postgreManager.DeleteAsync(request);
        }
    }
}
