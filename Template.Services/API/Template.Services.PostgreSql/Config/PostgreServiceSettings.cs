﻿namespace Template.Services.PostgreSql.Config
{
    public class PostgreServiceSettings
    {
        /// <summary>
        /// RabbitMq connection string
        /// </summary>
        public string RabbitMqConnection { get; set; }

        /// <summary>
        /// PostgreSql connection string
        /// </summary>
        public string PostgreSqlConnection { get; set; }
    }
}
