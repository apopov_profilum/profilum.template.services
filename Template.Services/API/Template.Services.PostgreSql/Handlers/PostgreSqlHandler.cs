﻿using System;
using System.Threading.Tasks;
using Template.Services.Core.Constants;
using Template.Services.Dtos.Base.Requests;
using Template.Services.Dtos.Base.Responses;
using Template.Services.Dtos.PostgreSql.Requests;
using Template.Services.Dtos.PostgreSql.Responses;
using Template.Services.PostgreSql.Handlers.Interfaces;
using Template.Services.PostgreSql.Managers.Interfaces;

namespace Template.Services.PostgreSql.Handlers
{
    public class PostgreSqlHandler : IPostgreSqlHandler
    {
        private readonly IPostgreSqlManager _sqlManager;

        public PostgreSqlHandler(IPostgreSqlManager sqlManager)
        {
            _sqlManager = sqlManager;
        }

        public async Task<CreateDbRecordResponse> CreateAsync(CreateDbRecordRequest request) => await HandleActionAsync(request, (r) => _sqlManager.CreateAsync(request));

        public async Task<GetDbRecordResponse> GetOneAsync(GetDbRecordRequest request) => await HandleActionAsync(request, (r) => _sqlManager.GetOneAsync(request));

        public async Task<GetDbRecordsResponse> GetSomeAsync(GetDbRecordsRequest request) => await HandleActionAsync(request, (r) => _sqlManager.GetSomeAsync(request));

        public async Task<UpdateDbRecordResponse> UpdateAsync(UpdateDbRecordRequest request) => await HandleActionAsync(request, (r) => _sqlManager.UpdateAsync(request));

        public async Task<DeleteDbRecordResponse> DeleteAsync(DeleteDbRecordRequest request) => await HandleActionAsync(request, (r) => _sqlManager.DeleteAsync(request));


        /// <summary>
        /// Base method to handle action
        /// </summary>
        /// <typeparam name="TRequest">Request type</typeparam>
        /// <typeparam name="TResponse">Response type</typeparam>
        /// <param name="request"></param>
        /// <param name="action"></param>
        /// <returns></returns>
        private async Task<TResponse> HandleActionAsync<TRequest, TResponse>(TRequest request, Func<TRequest, Task<TResponse>> action) where TRequest : BaseRequest, new()
                                                                                                                                       where TResponse : BaseResponse, new()
        {
            try
            {
                return await action(request);
            }
            catch (Exception ex)
            {
                return new TResponse()
                {
                    Comment = ex.Message,
                    Status = ResponseStatus.Failed
                };
            }
        }
    }
}
