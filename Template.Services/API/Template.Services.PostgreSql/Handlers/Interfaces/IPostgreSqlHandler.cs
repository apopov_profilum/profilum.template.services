﻿using System.Threading.Tasks;
using Template.Services.Dtos.PostgreSql.Requests;
using Template.Services.Dtos.PostgreSql.Responses;

namespace Template.Services.PostgreSql.Handlers.Interfaces
{
    public interface IPostgreSqlHandler
    {
        Task<CreateDbRecordResponse> CreateAsync(CreateDbRecordRequest request);

        Task<GetDbRecordResponse> GetOneAsync(GetDbRecordRequest request);

        Task<GetDbRecordsResponse> GetSomeAsync(GetDbRecordsRequest request);

        Task<UpdateDbRecordResponse> UpdateAsync(UpdateDbRecordRequest request);

        Task<DeleteDbRecordResponse> DeleteAsync(DeleteDbRecordRequest request);
    }
}
