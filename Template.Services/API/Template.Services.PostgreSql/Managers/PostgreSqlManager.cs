﻿using AutoMapper;
using Microsoft.EntityFrameworkCore;
using Serilog;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Template.Services.Core.Constants;
using Template.Services.Dtos.PostgreSql.Dto;
using Template.Services.Dtos.PostgreSql.Requests;
using Template.Services.Dtos.PostgreSql.Responses;
using Template.Services.PostgreSql.DataAccess.Interfaces;
using Template.Services.PostgreSql.Managers.Interfaces;
using Template.Services.PostgreSql.Models;

namespace Template.Services.PostgreSql.Managers
{
    public class PostgreSqlManager : IPostgreSqlManager
    {
        private readonly IPostgreSqlContext _db;
        private readonly ILogger _logger;
        private readonly IMapper _mapper;

        public PostgreSqlManager(IPostgreSqlContext db,
                                 ILogger logger,
                                 IMapper mapper)
        {
            _db = db;
            _logger = logger;
            _mapper = mapper;
        }

        /// <summary>
        /// Create record
        /// </summary>
        /// <returns>SchoolClass</returns>
        public async Task<CreateDbRecordResponse> CreateAsync(CreateDbRecordRequest request)
        {
            var dbRecord = _mapper.Map<DbRecordDto, DbRecord>(request.Record);
            _db.DbRecords.Add(dbRecord);
            await _db.SaveChangesAsync();

            return new CreateDbRecordResponse
            {
                Status = ResponseStatus.Success,
                Id = dbRecord.Id
            };
        }

        /// <summary>
        /// Get one record
        /// </summary>
        /// <returns>SchoolClass</returns>
        public async Task<GetDbRecordResponse> GetOneAsync(GetDbRecordRequest request)
        {
            var dbRecord = await _db.DbRecords.FirstOrDefaultAsync(up => up.Id == request.Id);
            if (dbRecord == null)
            {
                return new GetDbRecordResponse
                {
                    Status = ResponseStatus.NotFound,
                    Comment = $"Can't find DbRecord with Id = {request.Id}"
                };
            }

            return new GetDbRecordResponse
            {
                Status = ResponseStatus.Success,
                Record = _mapper.Map<DbRecord, DbRecordDto>(dbRecord)
            };
        }

        /// <summary>
        /// Get some records
        /// </summary>
        /// <returns>SchoolClass</returns>
        public async Task<GetDbRecordsResponse> GetSomeAsync(GetDbRecordsRequest request)
        {
            var dbRecords = await _db.DbRecords.Take(10).ToListAsync();

            return new GetDbRecordsResponse
            {
                Status = ResponseStatus.Success,
                Records = dbRecords.Select(r => _mapper.Map<DbRecord,DbRecordDto>(r)).ToList()
            };
        }


        /// <summary>
        /// Delete record
        /// </summary>
        /// <returns>Delete response</returns>
        public async Task<DeleteDbRecordResponse> DeleteAsync(DeleteDbRecordRequest request)
        {
            var dbRecord = await _db.DbRecords.FirstOrDefaultAsync(up => up.Id == request.Id);
            if (dbRecord == null)
            {
                return new DeleteDbRecordResponse
                {
                    Status = ResponseStatus.NotFound,
                    Comment = $"Can't find DbRecord with Id = {request.Id}"
                };
            }

            _db.DbRecords.Remove(dbRecord);
            await _db.SaveChangesAsync();

            return new DeleteDbRecordResponse()
            {
                Status = ResponseStatus.Success,
                Comment = "DbRecord deleted"
            };
        }

        /// <summary>
        /// Update record
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        public async Task<UpdateDbRecordResponse> UpdateAsync(UpdateDbRecordRequest request)
        {
            var currentValue = await _db.DbRecords.FirstOrDefaultAsync(up => up.Id == request.Record.Id);
            if (currentValue == null)
            {
                return new UpdateDbRecordResponse
                {
                    Status = ResponseStatus.NotFound,
                    Comment = $"Can't find DbRecord with Id = {request.Record.Id}"
                };
            }

            var updatedValue = _mapper.Map<DbRecordDto, DbRecord>(request.Record);

            currentValue = _mapper.Map(updatedValue, currentValue);

            _db.DbRecords.Update(currentValue);
            await _db.SaveChangesAsync();

            return new UpdateDbRecordResponse
            {
                Status = ResponseStatus.Success,
            };
        }
    }
}
