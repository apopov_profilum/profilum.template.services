﻿using AutoMapper;
using Template.Services.Dtos.PostgreSql.Dto;
using Template.Services.PostgreSql.Models;

namespace Template.Services.PostgreSql.Mappings
{
    public class MappingProfile: Profile
    {
        public MappingProfile()
        {
            CreateMap<DbRecord, DbRecordDto>().ReverseMap();

            CreateMap<DbRecord, DbRecord>()
                .ForMember(up => up.Id, opt => opt.Ignore())
                .ForMember(up => up.CreatedDate, opt => opt.Ignore())
                .ForAllOtherMembers(opt => opt.Condition(
                    (source, destination, sourceMember, destMember) => (sourceMember != null)));
        }
    }
}
