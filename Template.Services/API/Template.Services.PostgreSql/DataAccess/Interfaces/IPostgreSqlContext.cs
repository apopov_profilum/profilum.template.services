﻿using Microsoft.EntityFrameworkCore;
using Template.Services.PostgreSql.Models;

namespace Template.Services.PostgreSql.DataAccess.Interfaces
{
    public interface IPostgreSqlContext : IDbContext
    {
        DbSet<DbRecord> DbRecords { get; set; }
    }
}
