﻿using JetBrains.Annotations;
using Microsoft.EntityFrameworkCore.ChangeTracking;
using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;

namespace Template.Services.PostgreSql.DataAccess.Interfaces
{
    public interface IDbContext
    {
        EntityEntry Add([NotNull] object entity);
        EntityEntry<TEntity> Add<TEntity>([NotNull] TEntity entity) where TEntity : class;
        Task<EntityEntry> AddAsync([NotNull] object entity, CancellationToken cancellationToken = default(CancellationToken));
        Task<EntityEntry<TEntity>> AddAsync<TEntity>([NotNull] TEntity entity, CancellationToken cancellationToken = default(CancellationToken)) where TEntity : class;
        void AddRange([NotNull] IEnumerable<object> entities);
        void AddRange([NotNull] params object[] entities);
        Task AddRangeAsync([NotNull] IEnumerable<object> entities, CancellationToken cancellationToken = default(CancellationToken));
        Task AddRangeAsync([NotNull] params object[] entities);
        EntityEntry<TEntity> Attach<TEntity>([NotNull] TEntity entity) where TEntity : class;
        EntityEntry Attach([NotNull] object entity);
        void AttachRange([NotNull] IEnumerable<object> entities);
        void AttachRange([NotNull] params object[] entities);
        EntityEntry<TEntity> Entry<TEntity>([NotNull] TEntity entity) where TEntity : class;
        EntityEntry Entry([NotNull] object entity);
        TEntity Find<TEntity>([NotNull] params object[] keyValues) where TEntity : class;
        object Find([NotNull] Type entityType, [NotNull] params object[] keyValues);
        Task<TEntity> FindAsync<TEntity>([NotNull] params object[] keyValues) where TEntity : class;
        Task<object> FindAsync([NotNull] Type entityType, [NotNull] params object[] keyValues);
        Task<TEntity> FindAsync<TEntity>([NotNull] object[] keyValues, CancellationToken cancellationToken) where TEntity : class;
        Task<object> FindAsync([NotNull] Type entityType, [NotNull] object[] keyValues, CancellationToken cancellationToken);
        EntityEntry Remove([NotNull] object entity);
        EntityEntry<TEntity> Remove<TEntity>([NotNull] TEntity entity) where TEntity : class;
        void RemoveRange([NotNull] IEnumerable<object> entities);
        void RemoveRange([NotNull] params object[] entities);
        int SaveChanges();
        int SaveChanges(bool acceptAllChangesOnSuccess);
        Task<int> SaveChangesAsync(bool acceptAllChangesOnSuccess, CancellationToken cancellationToken = default(CancellationToken));
        Task<int> SaveChangesAsync(CancellationToken cancellationToken = default(CancellationToken));
        EntityEntry Update([NotNull] object entity);
        EntityEntry<TEntity> Update<TEntity>([NotNull] TEntity entity) where TEntity : class;
        void UpdateRange([NotNull] params object[] entities);
        void UpdateRange([NotNull] IEnumerable<object> entities);
    }
}
