﻿using Microsoft.EntityFrameworkCore;
using Template.Services.PostgreSql.DataAccess.Interfaces;
using Template.Services.PostgreSql.Models;

namespace Template.Services.PostgreSql.DataAccess
{
    public class PostgreSqlContext : DbContext, IPostgreSqlContext
    {
        public PostgreSqlContext(DbContextOptions<PostgreSqlContext> options)
            : base(options)
        { }

        public DbSet<DbRecord> DbRecords { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<DbRecord>().ToTable(nameof(DbRecords));
        }
    }
}
