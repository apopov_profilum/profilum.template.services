﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;
using AutoMapper;
using CorrelationId;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Serilog;
using Swashbuckle.AspNetCore.SwaggerGen;
using Template.Services.Core.Logging.SerilogEnrichers;
using Template.Services.Core.Swagger;
using Template.Services.Core.Swagger.DocumentFilters;
using Template.Services.Core.Swagger.OperationFilters;
using Template.Services.PostgreSql.Config;
using Template.Services.PostgreSql.DataAccess;
using Template.Services.PostgreSql.DataAccess.Interfaces;
using Template.Services.PostgreSql.Infrastructure;

namespace Profilum.Services.PostgreSql
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddCorrelationId();

            services.Configure<PostgreServiceSettings>(Configuration.GetSection(nameof(PostgreServiceSettings)));

            var documentsConfigSection = Configuration.GetSection(nameof(PostgreServiceSettings)).Get<PostgreServiceSettings>();

            services.RegisterEasyNetQ(documentsConfigSection.RabbitMqConnection);

            services.AddDbContext<PostgreSqlContext>(options => options.UseNpgsql(documentsConfigSection.PostgreSqlConnection));
            services.AddTransient<IPostgreSqlContext>(provider => provider.GetService<PostgreSqlContext>());

            services.AddMvc().SetCompatibilityVersion(CompatibilityVersion.Version_2_2);

            services.AddAutoMapper(Assembly.GetEntryAssembly());
            services.AddRouting(options => { options.LowercaseUrls = true; options.AppendTrailingSlash = false; });

            services.AddApiVersioning(o => o.ReportApiVersions = true);

            // DI for Handlers and Managers
            services.AddBusinessLogic();

            var logger = new LoggerConfiguration()
                .Enrich.With(new CorrelationIdEnricher(services.BuildServiceProvider().GetService<ICorrelationContextAccessor>()))
                .ReadFrom.Configuration(Configuration)
                .CreateLogger();
            services.AddSingleton<ILogger>(logger);

            services.AddLogging(builder => builder.AddSerilog(logger));

            services.AddSwaggerGen(c =>
            {
                c.SwaggerDoc("v1.0", SwaggerApiInfoGetter.CreateInfoForApiVersion("v1.0", "PostgreSql"));

                c.DocInclusionPredicate((docName, apiDesc) =>
                {
                    if (!apiDesc.TryGetMethodInfo(out MethodInfo methodInfo)) return false;

                    var versions = methodInfo.DeclaringType
                        .GetCustomAttributes(true)
                        .OfType<ApiVersionAttribute>()
                        .SelectMany(attr => attr.Versions);

                    return versions.Any(v => $"v{v.ToString()}" == docName);
                });

                c.OperationFilter<RemoveVersionOperationFilter>();
                c.OperationFilter<CorrelationFieldsOperationFilter>();

                c.DocumentFilter<SetVersionInPathDocumentFilter>();
                c.DocumentFilter<LowercaseDocumentFilter>();

                c.IncludeXmlComments(Path.Combine($"{AppContext.BaseDirectory}", $"{Assembly.GetEntryAssembly().GetName().Name}.xml"));
                c.DescribeAllEnumsAsStrings();
            });
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            app.UseCorrelationId(new CorrelationIdOptions
            {
                UseGuidForCorrelationId = true,
                UpdateTraceIdentifier = false
            });

            app.UseMvc();
            app.ConfigureEventBus();

            app.UseSwagger(c =>
            {
                c.RouteTemplate = "api/docs/{documentName}/swagger.json";
            });

            if (env.IsDevelopment())
            {
                app.UseSwaggerUI(c =>
                {
                    c.RoutePrefix = "api/docs";

                    c.SwaggerEndpoint("/api/docs/v1.0/swagger.json", "v1.0");
                });
            }
        }
    }
}
