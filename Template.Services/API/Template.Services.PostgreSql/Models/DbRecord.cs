﻿using System;

namespace Template.Services.PostgreSql.Models
{
    public class DbRecord
    {
        /// <summary>
        /// Need for correctly EF mapping
        /// </summary>
        public DbRecord()
        {
        }

        public Guid Id { get; set; }

        public string Name { get; set; }

        public string Description { get; set; }

        public DateTime CreatedDate { get; set; }
    }
}
