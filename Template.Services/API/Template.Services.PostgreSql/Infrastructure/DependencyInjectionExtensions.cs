﻿using Microsoft.Extensions.DependencyInjection;
using Template.Services.PostgreSql.Handlers;
using Template.Services.PostgreSql.Handlers.Interfaces;
using Template.Services.PostgreSql.Managers;
using Template.Services.PostgreSql.Managers.Interfaces;

namespace Template.Services.PostgreSql.Infrastructure
{
    public static class DependencyInjectionExtentions
    {
        public static void AddBusinessLogic(this IServiceCollection services)
        {
            services.AddTransient<IPostgreSqlHandler, PostgreSqlHandler>();
            services.AddTransient<IPostgreSqlManager, PostgreSqlManager>();
        }
    }
}
