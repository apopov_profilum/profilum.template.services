﻿using Microsoft.AspNetCore.Builder;
using Microsoft.Extensions.DependencyInjection;
using Template.Services.Dtos.PostgreSql.Requests;
using Template.Services.PostgreSql.Managers.Interfaces;

namespace Template.Services.PostgreSql.Infrastructure
{
    public static class RabbitMqSubscriptionExtensions
    {
        public static void ConfigureEventBus(this IApplicationBuilder builder)
        {
            var eventBus = builder.ApplicationServices.GetRequiredService<EasyNetQ.IBus>();

            eventBus.RespondAsync(async (CreateDbRecordRequest request) => await builder.ApplicationServices.CreateScope().ServiceProvider.GetService<IPostgreSqlManager>().CreateAsync(request));
            eventBus.RespondAsync(async (GetDbRecordRequest request) => await builder.ApplicationServices.CreateScope().ServiceProvider.GetService<IPostgreSqlManager>().GetOneAsync(request));
            eventBus.RespondAsync(async (GetDbRecordsRequest request) => await builder.ApplicationServices.CreateScope().ServiceProvider.GetService<IPostgreSqlManager>().GetSomeAsync(request));
            eventBus.RespondAsync(async (UpdateDbRecordRequest request) => await builder.ApplicationServices.CreateScope().ServiceProvider.GetService<IPostgreSqlManager>().UpdateAsync(request));
            eventBus.RespondAsync(async (DeleteDbRecordRequest request) => await builder.ApplicationServices.CreateScope().ServiceProvider.GetService<IPostgreSqlManager>().DeleteAsync(request));
        }
    }
}
