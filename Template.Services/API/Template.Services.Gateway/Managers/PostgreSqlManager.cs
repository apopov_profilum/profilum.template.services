﻿using System.Threading.Tasks;
using Template.Services.Dtos.PostgreSql.Requests;
using Template.Services.Dtos.PostgreSql.Responses;
using Template.Services.Gateway.Communication.MessageBrokers.Interfaces;
using Template.Services.Gateway.Managers.Interfaces;

namespace Template.Services.Gateway.Managers
{
    public class PostgreSqlManager : IPostgreSqlManager
    {
        private readonly IMessageBrokerManager _messageBrokerManager;

        public PostgreSqlManager(IMessageBrokerManager messageBrokerManager)
        {
            _messageBrokerManager = messageBrokerManager;
        }

        public async Task<CreateDbRecordResponse> CreateDbRecordAsync(CreateDbRecordRequest request)
        {
            return await _messageBrokerManager.SendRequestAsync<CreateDbRecordRequest, CreateDbRecordResponse>(request);
        }

        public async Task<GetDbRecordResponse> GetDbRecordAsync(GetDbRecordRequest request)
        {
            return await _messageBrokerManager.SendRequestAsync<GetDbRecordRequest, GetDbRecordResponse>(request);
        }

        public async Task<GetDbRecordsResponse> GetDbRecordsAsync(GetDbRecordsRequest request)
        {
            return await _messageBrokerManager.SendRequestAsync<GetDbRecordsRequest, GetDbRecordsResponse>(request);
        }

        public async Task<UpdateDbRecordResponse> UpdateDbRecordAsync(UpdateDbRecordRequest request)
        {
            return await _messageBrokerManager.SendRequestAsync<UpdateDbRecordRequest, UpdateDbRecordResponse>(request);
        }

        public async Task<DeleteDbRecordResponse> DeleteDbRecordAsync(DeleteDbRecordRequest request)
        {
            return await _messageBrokerManager.SendRequestAsync<DeleteDbRecordRequest, DeleteDbRecordResponse>(request);
        }
    }
}
