﻿using System.Threading.Tasks;
using Template.Services.Dtos.MongoDb.Requests;
using Template.Services.Dtos.MongoDb.Responses;

namespace Template.Services.Gateway.Managers.Interfaces
{
    public interface IMongoDbManager
    {
        Task<CreateMongoDocumentResponse> CreateMongoDocumentAsync(CreateMongoDocumentRequest request);

        Task<GetMongoDocumentResponse> GetMongoDocumentAsync(GetMongoDocumentRequest request);

        Task<GetMongoDocumentsResponse> GetMongoDocumentsAsync(GetMongoDocumentsRequest request);

        Task<UpdateMongoDocumentResponse> UpdateMongoDocumentAsync(UpdateMongoDocumentRequest request);

        Task<DeleteMongoDocumentResponse> DeleteMongoDocumentAsync(DeleteMongoDocumentRequest request);
    }
}
