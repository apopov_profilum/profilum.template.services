﻿using System.Threading.Tasks;
using Template.Services.Dtos.PostgreSql.Requests;
using Template.Services.Dtos.PostgreSql.Responses;

namespace Template.Services.Gateway.Managers.Interfaces
{
    public interface IPostgreSqlManager
    {
        Task<CreateDbRecordResponse> CreateDbRecordAsync(CreateDbRecordRequest request);

        Task<GetDbRecordResponse> GetDbRecordAsync(GetDbRecordRequest request);

        Task<GetDbRecordsResponse> GetDbRecordsAsync(GetDbRecordsRequest request);

        Task<UpdateDbRecordResponse> UpdateDbRecordAsync(UpdateDbRecordRequest request);

        Task<DeleteDbRecordResponse> DeleteDbRecordAsync(DeleteDbRecordRequest request);
    }
}
