﻿using System.Threading.Tasks;
using Template.Services.Dtos.Elastic.Requests;
using Template.Services.Dtos.Elastic.Responses;

namespace Template.Services.Gateway.Managers.Interfaces
{
    public interface IElasticManager
    {
        Task<CreateElasticDocumentResponse> CreateElasticDocumentAsync(CreateElasticDocumentRequest request);

        Task<GetElasticDocumentResponse> GetElasticDocumentAsync(GetElasticDocumentRequest request);

        Task<GetElasticDocumentsResponse> GetElasticDocumentsAsync(GetElasticDocumentsRequest request);

        Task<UpdateElasticDocumentResponse> UpdateElasticDocumentAsync(UpdateElasticDocumentRequest request);

        Task<DeleteElasticDocumentResponse> DeleteElasticDocumentAsync(DeleteElasticDocumentRequest request);
    }
}
