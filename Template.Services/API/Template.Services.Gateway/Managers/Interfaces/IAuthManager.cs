﻿using System.Threading.Tasks;
using Template.Services.Dtos.Auth.Requests;
using Template.Services.Dtos.Auth.Responses;

namespace Template.Services.Gateway.Managers.Interfaces
{
    public interface IAuthManager
    {
        Task<RegisterUserResponse> RegisterUserAsync(RegisterUserRequest request);

        Task<ChangePasswordResponse> ChangePasswordAsync(ChangePasswordRequest request);

        Task<GetPasswordResetTokenResponse> GetPasswordResetTokenAsync(GetPasswordResetTokenRequest request);

        Task<ResetPasswordResponse> ResetPasswordAsync(ResetPasswordRequest request);
    }
}
