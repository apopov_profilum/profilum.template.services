﻿using EasyNetQ;
using System.Threading.Tasks;
using Template.Services.Dtos.Elastic.Requests;
using Template.Services.Dtos.Elastic.Responses;
using Template.Services.Gateway.Communication.MessageBrokers.Interfaces;
using Template.Services.Gateway.Managers.Interfaces;

namespace Template.Services.Gateway.Managers
{
    public class ElasticManager : IElasticManager
    {
        private readonly IMessageBrokerManager _messageBrokerManager;

        public ElasticManager(IMessageBrokerManager messageBrokerManager)
        {
            _messageBrokerManager = messageBrokerManager;
        }

        public async Task<CreateElasticDocumentResponse> CreateElasticDocumentAsync(CreateElasticDocumentRequest request)
        {
            return await _messageBrokerManager.SendRequestAsync<CreateElasticDocumentRequest, CreateElasticDocumentResponse>(request);
        }

        public async Task<GetElasticDocumentResponse> GetElasticDocumentAsync(GetElasticDocumentRequest request)
        {
            return await _messageBrokerManager.SendRequestAsync<GetElasticDocumentRequest, GetElasticDocumentResponse>(request);
        }

        public async Task<GetElasticDocumentsResponse> GetElasticDocumentsAsync(GetElasticDocumentsRequest request)
        {
            return await _messageBrokerManager.SendRequestAsync<GetElasticDocumentsRequest, GetElasticDocumentsResponse>(request);
        }

        public async Task<UpdateElasticDocumentResponse> UpdateElasticDocumentAsync(UpdateElasticDocumentRequest request)
        {
            return await _messageBrokerManager.SendRequestAsync<UpdateElasticDocumentRequest, UpdateElasticDocumentResponse>(request);
        }

        public async Task<DeleteElasticDocumentResponse> DeleteElasticDocumentAsync(DeleteElasticDocumentRequest request)
        {
            return await _messageBrokerManager.SendRequestAsync<DeleteElasticDocumentRequest, DeleteElasticDocumentResponse>(request);
        }
    }
}
