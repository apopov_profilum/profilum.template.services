﻿using System.Threading.Tasks;
using Template.Services.Dtos.MongoDb.Requests;
using Template.Services.Dtos.MongoDb.Responses;
using Template.Services.Gateway.Communication.MessageBrokers.Interfaces;
using Template.Services.Gateway.Managers.Interfaces;

namespace Template.Services.Gateway.Managers
{
    public class MongoDbManager : IMongoDbManager
    {
        private readonly IMessageBrokerManager _messageBrokerManager;

        public MongoDbManager(IMessageBrokerManager messageBrokerManager)
        {
            _messageBrokerManager = messageBrokerManager;
        }

        public async Task<CreateMongoDocumentResponse> CreateMongoDocumentAsync(CreateMongoDocumentRequest request)
        {
            return await _messageBrokerManager.SendRequestAsync<CreateMongoDocumentRequest, CreateMongoDocumentResponse>(request);
        }

        public async Task<GetMongoDocumentResponse> GetMongoDocumentAsync(GetMongoDocumentRequest request)
        {
            return await _messageBrokerManager.SendRequestAsync<GetMongoDocumentRequest, GetMongoDocumentResponse>(request);
        }

        public async Task<GetMongoDocumentsResponse> GetMongoDocumentsAsync(GetMongoDocumentsRequest request)
        {
            return await _messageBrokerManager.SendRequestAsync<GetMongoDocumentsRequest, GetMongoDocumentsResponse>(request);
        }

        public async Task<UpdateMongoDocumentResponse> UpdateMongoDocumentAsync(UpdateMongoDocumentRequest request)
        {
            return await _messageBrokerManager.SendRequestAsync<UpdateMongoDocumentRequest, UpdateMongoDocumentResponse>(request);
        }

        public async Task<DeleteMongoDocumentResponse> DeleteMongoDocumentAsync(DeleteMongoDocumentRequest request)
        {
            return await _messageBrokerManager.SendRequestAsync<DeleteMongoDocumentRequest, DeleteMongoDocumentResponse>(request);
        }
    }
}
