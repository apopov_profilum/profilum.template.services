﻿using EasyNetQ;
using System;
using System.Threading.Tasks;
using Template.Services.Dtos.Auth.Requests;
using Template.Services.Dtos.Auth.Responses;
using Template.Services.Gateway.Communication.MessageBrokers.Interfaces;
using Template.Services.Gateway.Managers.Interfaces;

namespace Template.Services.Gateway.Managers
{
    public class AuthManager: IAuthManager
    {
        private readonly IMessageBrokerManager _messageBrokerManager;

        public AuthManager(IMessageBrokerManager messageBrokerManager)
        {
            _messageBrokerManager = messageBrokerManager;
        }

        public Task<ChangePasswordResponse> ChangePasswordAsync(ChangePasswordRequest request)
        {
            throw new NotImplementedException();
        }

        public Task<GetPasswordResetTokenResponse> GetPasswordResetTokenAsync(GetPasswordResetTokenRequest request)
        {
            throw new NotImplementedException();
        }

        public Task<RegisterUserResponse> RegisterUserAsync(RegisterUserRequest request)
        {
            throw new NotImplementedException();
        }

        public async Task<ResetPasswordResponse> ResetPasswordAsync(ResetPasswordRequest request)
        {
            return await _messageBrokerManager.SendRequestAsync<ResetPasswordRequest, ResetPasswordResponse>(request);
        }
    }
}
