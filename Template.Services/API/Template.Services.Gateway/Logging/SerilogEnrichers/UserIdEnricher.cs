﻿using Serilog.Core;
using Serilog.Events;
using System;
using Template.Services.Gateway.Middleware.DataGetters.Interfaces;

namespace Template.Services.Gateway.Logging.SerilogEnrichers
{
    /// <summary>
    /// Serilog output template {UserId} element 
    /// </summary>
    public class UserIdEnricher : ILogEventEnricher
    {
        private readonly IUserInformationGetter _userInformationGetter;

        private const string TagName = "UserId";
        private const string Unknown = "Unknown user";

        public UserIdEnricher(IUserInformationGetter userInformationGetter)
        {
            _userInformationGetter = userInformationGetter;
        }

        public void Enrich(LogEvent logEvent, ILogEventPropertyFactory propertyFactory)
        {
            var userId = _userInformationGetter.GetId();
            // Значение Unknown будет записано в 2-х случаях
            // 1) Метод закрыт авторизацией и к нему обратился неавторизованный пользователь
            // 2) Метод не закрыт авторизацией и к нему обратился авторизованный пользователь (т.к. значения в UserInformationGetter проставляются в Authorize атрибуте)
            logEvent.AddPropertyIfAbsent(propertyFactory.CreateProperty(
                    TagName, userId != Guid.Empty ? userId.ToString()
                                                  : Unknown));
        }
    }
}
