﻿using Microsoft.Extensions.DependencyInjection;
using Template.Services.Gateway.Managers;
using Template.Services.Gateway.Managers.Interfaces;

namespace Template.Services.Gateway.Infrastructure
{
    public static class BusinessLogicExtensions
    {
        public static void AddBusinessLogic(this IServiceCollection services)
        {
            services.AddTransient<IAuthManager, AuthManager>();
            services.AddTransient<IElasticManager, ElasticManager>();
            services.AddTransient<IMongoDbManager, MongoDbManager>();
            services.AddTransient<IPostgreSqlManager, PostgreSqlManager>();
        }          
    }
}
