﻿namespace Template.Services.Gateway.Config
{
    public class IdentitySettings
    {
        /// <summary>
        /// Client ID 
        /// </summary>
        public string ClientId { get; set; }

        /// <summary>
        /// Client secret 
        /// </summary>
        public string ClientSecret { get; set; }

        /// <summary>
        /// Scopes
        /// </summary>
        public string Scope { get; set; }

        /// <summary>
        /// Refresh token lifetime (in minutes)
        /// </summary>
        public int RefreshTokenLifeTimeMinutes { get; set; }

        /// <summary>
        /// Identity server address
        /// </summary>
        public string IdentityServerUrl { get; set; }

        /// <summary>
        /// Get Token address
        /// </summary>
        public string GetTokenUrl { get { return $"{IdentityServerUrl}/connect/token"; } }

        /// <summary>
        /// Revocation token url
        /// </summary>
        public string RevocationTokenUrl { get { return $"{IdentityServerUrl}/connect/revocation"; } }

        /// <summary>
        /// Sms code lifetime (in minutes)
        /// </summary>
        public int SmsCodeLifeTimeMinutes { get; set; }

        /// <summary>
        /// Send sms code to front
        /// </summary>
        public bool SendSmsCodeToFront { get; set; }
    }
}
