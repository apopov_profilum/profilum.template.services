﻿using System;

namespace Template.Services.Gateway.Models
{
    public class UserInformation
    {
        public string Email { get; set; }

        public string UserName { get; set; }

        public Guid UserId { get; set; }

        public string Role { get; set; }

        public string TokenKey { get; set; }
    }
}
