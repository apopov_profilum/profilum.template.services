﻿using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Http;
using Serilog;
using System;
using System.Diagnostics;
using System.Threading.Tasks;

namespace Template.Services.Gateway.Middleware
{
    /// <summary>
    /// Handle gateway request elapsed time middleware
    /// </summary>
	public class TimeLoggingMiddleware
    {
        private readonly RequestDelegate _next;
        private readonly ILogger _logger;
        private readonly string TimeLogTemplate = "[{0}] [{1}] Request finished in {2}ms";

        public TimeLoggingMiddleware(RequestDelegate next, ILogger logger)
        {
            _next = next;
            _logger = logger.ForContext<TimeLoggingMiddleware>();
        }

        public async Task Invoke(HttpContext httpContext)
        {
            var watch = Stopwatch.StartNew();
            try
            {
                await _next(httpContext);
            }
            catch (Exception ex)
            {
                _logger.Error(ex, ex.Message);
                throw ex;
            }
            finally
            {
                watch.Stop();
                var elapsedMs = watch.ElapsedMilliseconds;
                if (watch.ElapsedMilliseconds > 500)
                {
                    _logger.Warning(TimeLogTemplate, httpContext.Request.Method, httpContext.Request.Path, watch.ElapsedMilliseconds);
                }
                else
                {
                    _logger.Information(TimeLogTemplate, httpContext.Request.Method, httpContext.Request.Path, watch.ElapsedMilliseconds);
                }
            }
        }
    }

    public static class TimeLoggingMiddlewareExtensions
    {
        public static IApplicationBuilder UseTimeLoggingMiddleware(this IApplicationBuilder builder)
        {
            return builder.UseMiddleware<TimeLoggingMiddleware>();
        }
    }
}
