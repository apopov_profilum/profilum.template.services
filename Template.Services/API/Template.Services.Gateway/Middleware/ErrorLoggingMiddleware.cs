﻿using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Http;
using Newtonsoft.Json;
using Serilog;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using Template.Services.Core.Exceptions;
using Template.Services.Core.Models;

namespace Template.Services.Gateway.Middleware
{
    /// <summary>
    /// Handle gateway exception middleware
    /// </summary>
	public class ErrorLoggingMiddleware
    {
        private readonly RequestDelegate _next;
        private readonly ILogger _logger;

        private readonly string MethodPermissionDeniedError = "Permission denied";
        private readonly string UnauthorizedUserError = "Unauthorized user";

        public ErrorLoggingMiddleware(RequestDelegate next, ILogger logger)
        {
            _next = next;
            _logger = logger.ForContext<ErrorLoggingMiddleware>();
        }

        public async Task Invoke(HttpContext httpContext)
        {
            try
            {
                await _next(httpContext);
            }
            catch (Exception ex) when (ex.Message == MethodPermissionDeniedError || ex.Message == UnauthorizedUserError)
            {
                _logger.Warning(ex, ex.Message);
                await HandleExceptionAsync(httpContext, ex);
            }
            catch (Exception ex)
            {
                _logger.Error(ex, ex.Message);
                await HandleExceptionAsync(httpContext, ex);
            }
        }

        private static Task HandleExceptionAsync(HttpContext context, Exception exception)
        {
            var result = GetResponseFromException(exception, out int statusCode);

            context.Response.ContentType = "application/json";
            context.Response.StatusCode = statusCode;
            return context.Response.WriteAsync(result);
        }

        /// <summary>
        /// Get response body and http status code depending on exception
        /// </summary>
        /// <typeparam name="T">Exception type</typeparam>
        /// <param name="ex">current exception</param>
        /// <param name="statusCode">http status code</param>
        /// <returns></returns>
        private static string GetResponseFromException<T>(T ex, out int statusCode) where T : Exception
        {
            var exType = ex.GetType();
            statusCode = exType.IsAssignableFrom(typeof(HttpStatusCodeException)) ? (int)(ex as HttpStatusCodeException).StatusCode
                                                                                  : (int)HttpStatusCode.InternalServerError;

            return JsonConvert.SerializeObject(new ErrorResponse(ex, statusCode.ToString()));
        }
    }

    public static class ErrorLoggingMiddlewareExtensions
    {
        public static IApplicationBuilder UseErrorLoggingMiddleware(this IApplicationBuilder builder)
        {
            return builder.UseMiddleware<ErrorLoggingMiddleware>();
        }
    }
}
