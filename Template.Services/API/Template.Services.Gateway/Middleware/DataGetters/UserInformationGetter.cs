﻿using Microsoft.AspNetCore.Http;
using System;
using Template.Services.Gateway.Middleware.DataGetters.Interfaces;
using Template.Services.Gateway.Models;

namespace Template.Services.Gateway.Middleware.DataGetters
{
    public class UserInformationGetter : IUserInformationGetter
    {
        private const string UserInformationKey = "USER-INFORMATION";

        private readonly IHttpContextAccessor _contextAccessor;

        public UserInformationGetter(IHttpContextAccessor contextAccessor)
        {
            _contextAccessor = contextAccessor;
        }

        public Guid GetId() => GetUserInformation(_contextAccessor.HttpContext)?.UserId ?? Guid.Empty;

        /// <summary>
        /// Get UserInformation from current context
        /// </summary>
        /// <returns>Result can be null</returns>
        private UserInformation GetUserInformation(HttpContext context)
        {
            if (context == null || !context.Items.ContainsKey(UserInformationKey)) return null;

            return (UserInformation)context.Items[UserInformationKey];
        }
    }
}
