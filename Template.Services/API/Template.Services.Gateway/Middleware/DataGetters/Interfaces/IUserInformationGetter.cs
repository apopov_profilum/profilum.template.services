﻿using System;

namespace Template.Services.Gateway.Middleware.DataGetters.Interfaces
{
    public interface IUserInformationGetter
    {
        Guid GetId();
    }
}
