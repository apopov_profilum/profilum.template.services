﻿using Microsoft.AspNetCore.Mvc;
using System.Net;
using System.Threading.Tasks;
using Template.Services.Core.Constants;
using Template.Services.Dtos.MongoDb.Requests;
using Template.Services.Dtos.MongoDb.Responses;
using Template.Services.Gateway.Managers.Interfaces;

namespace Template.Services.Gateway.Controllers
{
    [Route("api/v{version:apiVersion}/[controller]")]
    [ApiVersion("1.0")]
    //[ApiController]
    public class MongoDbController : ControllerBase
    {
        private readonly IMongoDbManager _mongoManager;

        public MongoDbController(IMongoDbManager mongoManager)
        {
            _mongoManager = mongoManager;
        }

        /// <summary>
        /// Create elastic document
        /// </summary>
        /// <param name="request"></param>
        /// <response code="200">Ok</response>
        /// <response code="400">Operation failed</response>
        /// <response code="500">Operation failed</response>
        /// <returns></returns>
        [HttpPost]
        [Route("create")]
        [Produces(typeof(CreateMongoDocumentResponse))]
        [ProducesResponseType((int)HttpStatusCode.OK, Type = typeof(CreateMongoDocumentResponse))]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        [ProducesResponseType((int)HttpStatusCode.InternalServerError)]
        public async Task<IActionResult> Create([FromBody] CreateMongoDocumentRequest request)
        {
            var response = await _mongoManager.CreateMongoDocumentAsync(request);
            if (response.Status == ResponseStatus.Success)
            {
                return Ok(response);
            }
            else
            {
                return BadRequest(response);
            }
        }

        /// <summary>
        /// Get document
        /// </summary>
        /// <param name="request"></param>
        /// <response code="200">Ok</response>
        /// <response code="400">Operation failed</response>
        /// <response code="500">Operation failed</response>
        /// <returns></returns>
        [HttpGet]
        [Route("getone")]
        [Produces(typeof(GetMongoDocumentResponse))]
        [ProducesResponseType((int)HttpStatusCode.OK, Type = typeof(GetMongoDocumentResponse))]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        [ProducesResponseType((int)HttpStatusCode.InternalServerError)]
        public async Task<IActionResult> GetOne(GetMongoDocumentRequest request)
        {
            var response = await _mongoManager.GetMongoDocumentAsync(request);
            if (response.Status == ResponseStatus.Success)
            {
                return Ok(response);
            }
            else
            {
                return BadRequest(response);
            }
        }

        /// <summary>
        /// Get some documents
        /// </summary>
        /// <param name="request"></param>
        /// <response code="200">Ok</response>
        /// <response code="400">Operation failed</response>
        /// <response code="500">Operation failed</response>
        /// <returns></returns>
        [HttpGet]
        [Route("getsome")]
        [Produces(typeof(GetMongoDocumentsResponse))]
        [ProducesResponseType((int)HttpStatusCode.OK, Type = typeof(GetMongoDocumentsResponse))]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        [ProducesResponseType((int)HttpStatusCode.InternalServerError)]
        public async Task<IActionResult> GetSome(GetMongoDocumentsRequest request)
        {
            var response = await _mongoManager.GetMongoDocumentsAsync(request);
            if (response.Status == ResponseStatus.Success)
            {
                return Ok(response);
            }
            else
            {
                return BadRequest(response);
            }
        }

        /// <summary>
        /// Update elastic document
        /// </summary>
        /// <param name="request"></param>
        /// <response code="200">Ok</response>
        /// <response code="400">Operation failed</response>
        /// <response code="500">Operation failed</response>
        /// <returns></returns>
        [HttpPut]
        [Route("update")]
        [Produces(typeof(UpdateMongoDocumentResponse))]
        [ProducesResponseType((int)HttpStatusCode.OK, Type = typeof(UpdateMongoDocumentResponse))]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        [ProducesResponseType((int)HttpStatusCode.InternalServerError)]
        public async Task<IActionResult> Update([FromBody] UpdateMongoDocumentRequest request)
        {
            var response = await _mongoManager.UpdateMongoDocumentAsync(request);
            if (response.Status == ResponseStatus.Success)
            {
                return Ok(response);
            }
            else
            {
                return BadRequest(response);
            }
        }

        /// <summary>
        /// Update elastic document
        /// </summary>
        /// <param name="request"></param>
        /// <response code="200">Ok</response>
        /// <response code="400">Operation failed</response>
        /// <response code="500">Operation failed</response>
        /// <returns></returns>
        [HttpDelete]
        [Route("delete")]
        [Produces(typeof(DeleteMongoDocumentResponse))]
        [ProducesResponseType((int)HttpStatusCode.OK, Type = typeof(DeleteMongoDocumentResponse))]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        [ProducesResponseType((int)HttpStatusCode.InternalServerError)]
        public async Task<IActionResult> Delete(DeleteMongoDocumentRequest request)
        {
            var response = await _mongoManager.DeleteMongoDocumentAsync(request);
            if (response.Status == ResponseStatus.Success)
            {
                return Ok(response);
            }
            else
            {
                return BadRequest(response);
            }
        }
    }
}
