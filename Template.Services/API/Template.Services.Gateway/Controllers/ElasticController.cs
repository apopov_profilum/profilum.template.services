﻿using Microsoft.AspNetCore.Mvc;
using System.Net;
using System.Threading.Tasks;
using Template.Services.Core.Constants;
using Template.Services.Dtos.Elastic.Requests;
using Template.Services.Dtos.Elastic.Responses;
using Template.Services.Gateway.Managers.Interfaces;

namespace Template.Services.Gateway.Controllers
{
    [Route("api/v{version:apiVersion}/[controller]")]
    [ApiVersion("1.0")]
    //[ApiController]
    public class ElasticController : ControllerBase
    {
        private readonly IElasticManager _elasticManager;

        public ElasticController(IElasticManager elasticManager)
        {
            _elasticManager = elasticManager;
        }

        /// <summary>
        /// Create elastic document
        /// </summary>
        /// <param name="request"></param>
        /// <response code="200">Ok</response>
        /// <response code="400">Operation failed</response>
        /// <response code="500">Operation failed</response>
        /// <returns></returns>
        [HttpPost]
        [Route("create")]
        [Produces(typeof(CreateElasticDocumentResponse))]
        [ProducesResponseType((int)HttpStatusCode.OK, Type = typeof(CreateElasticDocumentResponse))]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        [ProducesResponseType((int)HttpStatusCode.InternalServerError)]
        public async Task<IActionResult> Create([FromBody] CreateElasticDocumentRequest request)
        {
            var response = await _elasticManager.CreateElasticDocumentAsync(request);
            if (response.Status == ResponseStatus.Success)
            {
                return Ok(response);
            }
            else
            {
                return BadRequest(response);
            }
        }

        /// <summary>
        /// Get document
        /// </summary>
        /// <param name="request"></param>
        /// <response code="200">Ok</response>
        /// <response code="400">Operation failed</response>
        /// <response code="500">Operation failed</response>
        /// <returns></returns>
        [HttpGet]
        [Route("getone")]
        [Produces(typeof(GetElasticDocumentResponse))]
        [ProducesResponseType((int)HttpStatusCode.OK, Type = typeof(GetElasticDocumentResponse))]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        [ProducesResponseType((int)HttpStatusCode.InternalServerError)]
        public async Task<IActionResult> GetOne(GetElasticDocumentRequest request)
        {
            var response = await _elasticManager.GetElasticDocumentAsync(request);
            if (response.Status == ResponseStatus.Success)
            {
                return Ok(response);
            }
            else
            {
                return BadRequest(response);
            }
        }

        /// <summary>
        /// Get some documents
        /// </summary>
        /// <param name="request"></param>
        /// <response code="200">Ok</response>
        /// <response code="400">Operation failed</response>
        /// <response code="500">Operation failed</response>
        /// <returns></returns>
        [HttpGet]
        [Route("getsome")]
        [Produces(typeof(GetElasticDocumentsResponse))]
        [ProducesResponseType((int)HttpStatusCode.OK, Type = typeof(GetElasticDocumentsResponse))]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        [ProducesResponseType((int)HttpStatusCode.InternalServerError)]
        public async Task<IActionResult> GetSome(GetElasticDocumentsRequest request)
        {
            var response = await _elasticManager.GetElasticDocumentsAsync(request);
            if (response.Status == ResponseStatus.Success)
            {
                return Ok(response);
            }
            else
            {
                return BadRequest(response);
            }
        }

        /// <summary>
        /// Update elastic document
        /// </summary>
        /// <param name="request"></param>
        /// <response code="200">Ok</response>
        /// <response code="400">Operation failed</response>
        /// <response code="500">Operation failed</response>
        /// <returns></returns>
        [HttpPut]
        [Route("update")]
        [Produces(typeof(UpdateElasticDocumentResponse))]
        [ProducesResponseType((int)HttpStatusCode.OK, Type = typeof(UpdateElasticDocumentResponse))]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        [ProducesResponseType((int)HttpStatusCode.InternalServerError)]
        public async Task<IActionResult> Update([FromBody] UpdateElasticDocumentRequest request)
        {
            var response = await _elasticManager.UpdateElasticDocumentAsync(request);
            if (response.Status == ResponseStatus.Success)
            {
                return Ok(response);
            }
            else
            {
                return BadRequest(response);
            }
        }

        /// <summary>
        /// Update elastic document
        /// </summary>
        /// <param name="request"></param>
        /// <response code="200">Ok</response>
        /// <response code="400">Operation failed</response>
        /// <response code="500">Operation failed</response>
        /// <returns></returns>
        [HttpDelete]
        [Route("delete")]
        [Produces(typeof(DeleteElasticDocumentResponse))]
        [ProducesResponseType((int)HttpStatusCode.OK, Type = typeof(DeleteElasticDocumentResponse))]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        [ProducesResponseType((int)HttpStatusCode.InternalServerError)]
        public async Task<IActionResult> Delete(DeleteElasticDocumentRequest request)
        {
            var response = await _elasticManager.DeleteElasticDocumentAsync(request);
            if (response.Status == ResponseStatus.Success)
            {
                return Ok(response);
            }
            else
            {
                return BadRequest(response);
            }
        }
    }
}
