﻿using Microsoft.AspNetCore.Mvc;
using System.Net;
using System.Threading.Tasks;
using Template.Services.Core.Constants;
using Template.Services.Dtos.PostgreSql.Requests;
using Template.Services.Dtos.PostgreSql.Responses;
using Template.Services.Gateway.Managers.Interfaces;

namespace Template.Services.Gateway.Controllers
{
    [Route("api/v{version:apiVersion}/[controller]")]
    [ApiVersion("1.0")]
    //[ApiController]
    public class PostgreSqlController : ControllerBase
    {
        private readonly IPostgreSqlManager _sqlManager;

        public PostgreSqlController(IPostgreSqlManager sqlManager)
        {
            _sqlManager = sqlManager;
        }

        /// <summary>
        /// Create elastic document
        /// </summary>
        /// <param name="request"></param>
        /// <response code="200">Ok</response>
        /// <response code="400">Operation failed</response>
        /// <response code="500">Operation failed</response>
        /// <returns></returns>
        [HttpPost]
        [Route("create")]
        [Produces(typeof(CreateDbRecordResponse))]
        [ProducesResponseType((int)HttpStatusCode.OK, Type = typeof(CreateDbRecordResponse))]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        [ProducesResponseType((int)HttpStatusCode.InternalServerError)]
        public async Task<IActionResult> Create([FromBody] CreateDbRecordRequest request)
        {
            var response = await _sqlManager.CreateDbRecordAsync(request);
            if (response.Status == ResponseStatus.Success)
            {
                return Ok(response);
            }
            else
            {
                return BadRequest(response);
            }
        }

        /// <summary>
        /// Get document
        /// </summary>
        /// <param name="request"></param>
        /// <response code="200">Ok</response>
        /// <response code="400">Operation failed</response>
        /// <response code="500">Operation failed</response>
        /// <returns></returns>
        [HttpGet]
        [Route("getone")]
        [Produces(typeof(GetDbRecordResponse))]
        [ProducesResponseType((int)HttpStatusCode.OK, Type = typeof(GetDbRecordResponse))]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        [ProducesResponseType((int)HttpStatusCode.InternalServerError)]
        public async Task<IActionResult> GetOne(GetDbRecordRequest request)
        {
            var response = await _sqlManager.GetDbRecordAsync(request);
            if (response.Status == ResponseStatus.Success)
            {
                return Ok(response);
            }
            else
            {
                return BadRequest(response);
            }
        }

        /// <summary>
        /// Get some documents
        /// </summary>
        /// <param name="request"></param>
        /// <response code="200">Ok</response>
        /// <response code="400">Operation failed</response>
        /// <response code="500">Operation failed</response>
        /// <returns></returns>
        [HttpGet]
        [Route("getsome")]
        [Produces(typeof(GetDbRecordsResponse))]
        [ProducesResponseType((int)HttpStatusCode.OK, Type = typeof(GetDbRecordsResponse))]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        [ProducesResponseType((int)HttpStatusCode.InternalServerError)]
        public async Task<IActionResult> GetSome(GetDbRecordsRequest request)
        {
            var response = await _sqlManager.GetDbRecordsAsync(request);
            if (response.Status == ResponseStatus.Success)
            {
                return Ok(response);
            }
            else
            {
                return BadRequest(response);
            }
        }

        /// <summary>
        /// Update elastic document
        /// </summary>
        /// <param name="request"></param>
        /// <response code="200">Ok</response>
        /// <response code="400">Operation failed</response>
        /// <response code="500">Operation failed</response>
        /// <returns></returns>
        [HttpPut]
        [Route("update")]
        [Produces(typeof(UpdateDbRecordResponse))]
        [ProducesResponseType((int)HttpStatusCode.OK, Type = typeof(UpdateDbRecordResponse))]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        [ProducesResponseType((int)HttpStatusCode.InternalServerError)]
        public async Task<IActionResult> Update([FromBody] UpdateDbRecordRequest request)
        {
            var response = await _sqlManager.UpdateDbRecordAsync(request);
            if (response.Status == ResponseStatus.Success)
            {
                return Ok(response);
            }
            else
            {
                return BadRequest(response);
            }
        }

        /// <summary>
        /// Update elastic document
        /// </summary>
        /// <param name="request"></param>
        /// <response code="200">Ok</response>
        /// <response code="400">Operation failed</response>
        /// <response code="500">Operation failed</response>
        /// <returns></returns>
        [HttpDelete]
        [Route("delete")]
        [Produces(typeof(DeleteDbRecordResponse))]
        [ProducesResponseType((int)HttpStatusCode.OK, Type = typeof(DeleteDbRecordResponse))]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        [ProducesResponseType((int)HttpStatusCode.InternalServerError)]
        public async Task<IActionResult> Delete(DeleteDbRecordRequest request)
        {
            var response = await _sqlManager.DeleteDbRecordAsync(request);
            if (response.Status == ResponseStatus.Success)
            {
                return Ok(response);
            }
            else
            {
                return BadRequest(response);
            }
        }
    }
}
