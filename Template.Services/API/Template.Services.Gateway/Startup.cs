﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;
using AutoMapper;
using CorrelationId;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Newtonsoft.Json;
using Serilog;
using Swashbuckle.AspNetCore.SwaggerGen;
using Template.Services.Core.Logging.Handlers;
using Template.Services.Core.Logging.SerilogEnrichers;
using Template.Services.Core.Swagger;
using Template.Services.Core.Swagger.DocumentFilters;
using Template.Services.Core.Swagger.OperationFilters;
using Template.Services.Gateway.Communication.MessageBrokers;
using Template.Services.Gateway.Communication.MessageBrokers.Interfaces;
using Template.Services.Gateway.Infrastructure;
using Template.Services.Gateway.Logging.SerilogEnrichers;
using Template.Services.Gateway.Middleware;
using Template.Services.Gateway.Middleware.DataGetters;
using Template.Services.Gateway.Middleware.DataGetters.Interfaces;

namespace Template.Services.Gateway
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddCorrelationId();
            services.AddMvc().SetCompatibilityVersion(CompatibilityVersion.Version_2_2)
                             .AddJsonOptions(options =>
                             {
                                 options.SerializerSettings.ReferenceLoopHandling = ReferenceLoopHandling.Ignore;
                             });

            services.AddAutoMapper(Assembly.GetEntryAssembly());
            services.AddRouting(options => { options.LowercaseUrls = true; options.AppendTrailingSlash = false; });

            services.AddApiVersioning(o => o.ReportApiVersions = true);

            services.Configure<Config.IdentitySettings>(Configuration.GetSection(nameof(Config.IdentitySettings)));

            services.AddTransient<CorrelationHandler>();

            //services.AddHttpClient<ITokenClient, TokenClient>()
            //        .AddHttpMessageHandler<CorrelationHandler>();

            services.AddDistributedRedisCache(option =>
            {
                option.Configuration = Configuration.GetConnectionString("RedisCacheServiceConnection");
            });

            services.RegisterEasyNetQ(Configuration.GetConnectionString("RabbitMqConnection"));

            services.AddSingleton<IHttpContextAccessor, HttpContextAccessor>();

            services.AddTransient<IMessageBrokerManager, RabbitManager>();
            // services.AddTransient<IStorageManager, DistributedCacheRedisManager>();

            services.AddScoped<IUserInformationGetter, UserInformationGetter>();
            // services.AddScoped<ITokenClaimsGetter, TokenClaimsGetter>();

            // services.AddTransient<ISessionValuesGetter, SessionValuesGetter>();


            var logger = new LoggerConfiguration()
                        .Enrich.With(new CorrelationIdEnricher(services.BuildServiceProvider().GetService<ICorrelationContextAccessor>()))
                        .Enrich.With(new UserIdEnricher(services.BuildServiceProvider().GetService<IUserInformationGetter>()))
                        .ReadFrom.Configuration(Configuration)
                        .MinimumLevel.Override("Microsoft.AspNetCore.Hosting.Internal.WebHost", Serilog.Events.LogEventLevel.Warning)
                        .CreateLogger();
            services.AddSingleton<Serilog.ILogger>(logger);
            services.AddLogging(builder => builder.AddSerilog(logger));

            services.AddSwaggerGen(c =>
            {
                c.SwaggerDoc("v1.0", SwaggerApiInfoGetter.CreateInfoForApiVersion("v1.0", "Gateway"));

                c.DocInclusionPredicate((docName, apiDesc) =>
                {
                    if (!apiDesc.TryGetMethodInfo(out MethodInfo methodInfo)) return false;

                    var versions = methodInfo.DeclaringType
                        .GetCustomAttributes(true)
                        .OfType<ApiVersionAttribute>()
                        .SelectMany(attr => attr.Versions);

                    return versions.Any(v => $"v{v.ToString()}" == docName);
                });

                c.OperationFilter<RemoveVersionOperationFilter>();
                c.OperationFilter<TokenKeyHeaderOperationFilter>();
                c.OperationFilter<CorrelationFieldsOperationFilter>();
                // c.OperationFilter<FileUploadOperation>();

                c.DocumentFilter<SetVersionInPathDocumentFilter>();
                c.DocumentFilter<LowercaseDocumentFilter>();

                c.IncludeXmlComments(Path.Combine($"{AppContext.BaseDirectory}", $"{Assembly.GetEntryAssembly().GetName().Name}.xml"));
                c.DescribeAllEnumsAsStrings();
            });

            services.AddAuthentication();
            // services.AddAuthorizationPolicies();
            services.AddBusinessLogic();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env, IApplicationLifetime appLifetime)
        {
            app.UseCorrelationId(new CorrelationIdOptions
            {
                UseGuidForCorrelationId = true,
                UpdateTraceIdentifier = false
            });

            appLifetime.ApplicationStopped.Register(Log.CloseAndFlush);

            app.UseTimeLoggingMiddleware();
            app.UseErrorLoggingMiddleware();

            app.UseAuthentication();
            app.UseMvc();

            app.UseSwagger(c =>
            {
                c.RouteTemplate = "api/docs/{documentName}/swagger.json";
            });

            if (env.IsDevelopment())
            {
                app.UseSwaggerUI(c =>
                {
                    c.RoutePrefix = "api/docs";

                    c.SwaggerEndpoint("/api/docs/v1.0/swagger.json", "v1.0");
                });
            }
        }
    }
}
