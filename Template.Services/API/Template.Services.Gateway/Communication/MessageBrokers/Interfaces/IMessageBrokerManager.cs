﻿using System.Threading.Tasks;
using Template.Services.Dtos.Base.Requests;
using Template.Services.Dtos.Base.Responses;

namespace Template.Services.Gateway.Communication.MessageBrokers.Interfaces
{
    public interface IMessageBrokerManager
    {
        Task<TResponse> SendRequestAsync<TRequest, TResponse>(TRequest request) where TRequest : BaseRequest
                                                                                where TResponse : BaseResponse;
    }
}
