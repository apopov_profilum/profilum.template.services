﻿using CorrelationId;
using EasyNetQ;
using Serilog;
using System;
using System.Threading.Tasks;
using Template.Services.Dtos.Base.Requests;
using Template.Services.Dtos.Base.Responses;
using Template.Services.Gateway.Communication.MessageBrokers.Interfaces;
using Template.Services.Gateway.Middleware.DataGetters.Interfaces;

namespace Template.Services.Gateway.Communication.MessageBrokers
{
    public class RabbitManager : IMessageBrokerManager
    {
        private readonly IBus _bus;
        private readonly ICorrelationContextAccessor _correlationContextAccessor;
        private readonly IUserInformationGetter _userInformationGetter;
        private readonly ILogger _logger;

        public RabbitManager(IBus bus,
                             ICorrelationContextAccessor correlationContextAccessor,
                             IUserInformationGetter userInformationGetter,
                             ILogger logger)
        {
            _bus = bus;
            _correlationContextAccessor = correlationContextAccessor;
            _userInformationGetter = userInformationGetter;
            _logger = logger;
        }

        /// <summary>
        /// Send request with type T1 to queue
        /// </summary>
        /// <typeparam name="TRequest">Request type</typeparam>
        /// <typeparam name="TResponse">Response type</typeparam>
        /// <param name="request"></param>
        /// <returns></returns>
        public async Task<TResponse> SendRequestAsync<TRequest, TResponse>(TRequest request) where TRequest : BaseRequest
                                                                                             where TResponse : BaseResponse
        {
            request.CorrelationId = GetCorrelationId();
            request.CorrelationUserId = _userInformationGetter.GetId();

            return await _bus.RequestAsync<TRequest, TResponse>(request);
        }

        private Guid GetCorrelationId()
        {
            var correlationId = Guid.Empty;
            Guid.TryParse(_correlationContextAccessor.CorrelationContext?.CorrelationId, out correlationId);
            return correlationId;
        }

    }
}
