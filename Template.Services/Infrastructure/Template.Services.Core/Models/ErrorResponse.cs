﻿using System;
using System.Reflection;

namespace Template.Services.Core.Models
{
    public class ErrorResponse
    {
        public Error Error { get; set; }

        public ErrorResponse() { }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="ex">исключение</param>
        /// <param name="code">код ошибки</param>
        /// <param name="details"></param>
        public ErrorResponse(Exception ex, string code, params Exception[] details)
        {
            var senderServiceName = Assembly.GetCallingAssembly().GetName().Name;
            Error = new Error(ex, code, senderServiceName, details);
        }
    }
}
