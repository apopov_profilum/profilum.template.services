﻿using System;
using System.Collections.Generic;

namespace Template.Services.Core.Models
{
    public class Error
    {
        public string Code { get; set; }

        public string Message { get; set; }

        public Error[] Details { get; set; }

        public Error InnerError { get; set; }

        public string SenderService { get; set; }

        public Error() { }

        public Error(Exception exception, string code, string senderService = null, params Exception[] details)
        {
            SenderService = senderService;
            Code = code;
            Message = exception.Message;
            if (exception.InnerException != null)
                InnerError = new Error(exception.InnerException, null);

            if (details != null && details.Length > 0)
            {
                var errors = new List<Error>();

                foreach (var ex in details)
                {
                    errors.Add(new Error(ex, null));
                }
            }
        }
    }
}
