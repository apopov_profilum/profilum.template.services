﻿namespace Template.Services.Core.Constants
{
    public static class ResponseStatus
    {
        public static readonly string Success = "Success";

        public static readonly string Failed = "Failed";

        public static readonly string NotFound = "Not Found";
    }
}
