﻿namespace Template.Services.Core.Constants
{
    public static class MarathonJwtClaimTypes
    {
        /// <summary>
        /// Does current user have profile?.
        /// </summary>
        public const string ProfileExists = "mrth_profileexists";
    }
}
