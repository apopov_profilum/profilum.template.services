﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Template.Services.Core.Utilities
{
    public static class ListExtensions
    {
        public static IEnumerable<IEnumerable<T>> Partition<T>(this ICollection<T> source, Int32 size)
        {
            for (int i = 0; i < Math.Ceiling(source.Count / (Double)size); i++)
                yield return new List<T>(source.Skip(size * i).Take(size));
        }
    }
}
