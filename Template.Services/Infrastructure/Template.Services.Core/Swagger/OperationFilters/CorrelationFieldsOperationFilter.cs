﻿using Swashbuckle.AspNetCore.Swagger;
using Swashbuckle.AspNetCore.SwaggerGen;
using System;
using System.Linq;

namespace Template.Services.Core.Swagger.OperationFilters
{
    /// <summary>
    /// Delete correlation fields from Swagger
    /// </summary>
    public class CorrelationFieldsOperationFilter : IOperationFilter
    {
        private readonly string[] CorrelationFieldNames = new string[] { "CorrelationId", "CorrelationUserId" }; // field names from BaseRequest

        public void Apply(Operation operation, OperationFilterContext context)
        {
            if (operation.Parameters != null)
            {
                operation.Parameters = operation.Parameters.Where(p => !CorrelationFieldNames.Contains(p.Name)).ToList();
            }
        }
    }
}
