﻿using Swashbuckle.AspNetCore.Swagger;
using Swashbuckle.AspNetCore.SwaggerGen;
using System.Linq;

namespace Template.Services.Core.Swagger.OperationFilters
{
    /// <summary>
    /// Фильтр для удаления версии сервиса из параметров
    /// </summary>
    public class RemoveVersionOperationFilter : IOperationFilter
    {
        /// <summary>
        /// Удалить версию сервиса из параметров операции
        /// </summary>
        /// <param name="operation">Операция</param>
        /// <param name="context">Контекст</param>
        public void Apply(Operation operation, OperationFilterContext context)
        {
            var versionParameter = operation.Parameters.Single(p => p.Name == "version");
            operation.Parameters.Remove(versionParameter);
        }
    }
}
