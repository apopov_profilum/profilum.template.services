﻿using System.Collections.Generic;
using System.Linq;
using Swashbuckle.AspNetCore.Swagger;
using Swashbuckle.AspNetCore.SwaggerGen;
using Microsoft.AspNetCore.Authorization;

namespace Template.Services.Core.Swagger.OperationFilters
{
    public class TokenKeyHeaderOperationFilter : IOperationFilter
    {
        private const string HeaderName = "tokenkey";

        public void Apply(Operation operation, OperationFilterContext context)
        {
            if (operation.Parameters == null)
            {
                operation.Parameters = new List<IParameter>();
            }

            var isExists = IsAuthorizeAttributeExists(context);
            if (isExists)
            {
                operation.Parameters.Add(new NonBodyParameter
                {
                    Name = HeaderName,
                    In = "header",
                    Type = "string",
                    Required = true
                });
            }
        }

        /// <summary>
        /// Is operation available only for authorized users?
        /// </summary>
        /// <param name="context">current operation context</param>
        /// <returns></returns>
        private bool IsAuthorizeAttributeExists(OperationFilterContext context)
        {
            return context.MethodInfo.CustomAttributes.Any(a => a.AttributeType == typeof(AuthorizeAttribute));
        }
    }
}
