﻿using System.Linq;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc.Controllers;
using Swashbuckle.AspNetCore.Swagger;
using Swashbuckle.AspNetCore.SwaggerGen;

namespace Template.Services.Core.Swagger.OperationFilters
{
    /// <summary>
    /// Add role requirement to swagger method description
    /// </summary>
    public class AuthRequirementOperationFilter : IOperationFilter
    {
        private const string Anonymous = "Anonymous";

        public void Apply(Operation operation, OperationFilterContext context)
        {
            var authAttribute = (context.ApiDescription.ActionDescriptor as ControllerActionDescriptor).MethodInfo.CustomAttributes
                .FirstOrDefault(ca => ca.AttributeType.UnderlyingSystemType == typeof(AllowAnonymousAttribute) || ca.AttributeType.UnderlyingSystemType == typeof(AuthorizeAttribute));
            if (authAttribute != null)
            {
                var rolePolicy = authAttribute.AttributeType.UnderlyingSystemType == typeof(AllowAnonymousAttribute)
                               ? Anonymous
                               : authAttribute.NamedArguments.Count == 1
                               ? authAttribute.NamedArguments.First().TypedValue.Value.ToString()
                               : null;

                if (rolePolicy != null)
                {
                    operation.Description = $"AuthorizeRequirement: {rolePolicy}";
                }
            }
        }
    }
}
