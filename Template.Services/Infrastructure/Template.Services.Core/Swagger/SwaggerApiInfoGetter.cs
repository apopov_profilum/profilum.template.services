﻿using Swashbuckle.AspNetCore.Swagger;

namespace Template.Services.Core.Swagger
{
    public static class SwaggerApiInfoGetter
    {
        /// <summary>
        /// Create api version description
        /// </summary>
        /// <param name="version">current api version</param>
        /// <param name="serviceName">service name</param>
        /// <param name="isDeprecated">api version is deprecated</param>
        /// <returns></returns>
        public static Info CreateInfoForApiVersion(string version, string serviceName, bool isDeprecated = false) =>
            new Info
            {
                Title = $"{serviceName} Service API {version}",
                Version = version,
                Description = !isDeprecated ? $"{serviceName} service"
                                            : $"Deprecated {serviceName} service",
                Contact = new Contact { Name = "Profliner Team", Email = "info@profliner.ru" },
            };
    }
}
