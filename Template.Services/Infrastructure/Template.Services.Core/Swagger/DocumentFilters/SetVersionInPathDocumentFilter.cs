﻿using System.Linq;
using Swashbuckle.AspNetCore.Swagger;
using Swashbuckle.AspNetCore.SwaggerGen;

namespace Template.Services.Core.Swagger.DocumentFilters
{
    /// <summary>
    /// Фильтр для добавление версии в маршрут к операции
    /// </summary>
    public class SetVersionInPathDocumentFilter : IDocumentFilter
    {
        /// <summary>
        /// Добавить версию в маршрут к операции
        /// </summary>
        /// <param name="swaggerDoc">Документ Swagger (описание операции)</param>
        /// <param name="context">Контекст</param>
        public void Apply(SwaggerDocument swaggerDoc, DocumentFilterContext context)
        {
            swaggerDoc.Paths = swaggerDoc.Paths
                .ToDictionary(
                    path => path.Key.Replace("v{version}", swaggerDoc.Info.Version),
                    path => path.Value
                );
        }
    }
}
