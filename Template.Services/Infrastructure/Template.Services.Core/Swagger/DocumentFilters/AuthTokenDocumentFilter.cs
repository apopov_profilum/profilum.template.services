﻿using Swashbuckle.AspNetCore.Swagger;
using Swashbuckle.AspNetCore.SwaggerGen;
using System.Collections.Generic;

namespace Template.Services.Core.Swagger.DocumentFilters
{
    /// <summary>
    /// Add Token methods to swagger
    /// </summary>
    public class AuthTokenDocumentFilter : IDocumentFilter
    {
        public void Apply(SwaggerDocument swaggerDoc, DocumentFilterContext context)
        {
            swaggerDoc.Paths.Add("/connect/token", GetTokenPathItem());
            swaggerDoc.Paths.Add("/connect/revocation", RevokeTokenPathItem());
        }

        private PathItem GetTokenPathItem()
        {
            return new PathItem
            {
                Post = new Operation
                {
                    Description = "1) Login by credentials: grant_type = 'password', scope = 'template_api offline_access', required fields: client_id, client_secret, username, password. \r\n" +
                                  "2) Get token by refresh token: grant_type = 'refresh_token', required fields: client_id, client_secret, refresh_token.",
                    Tags = new List<string> { "Token" },
                    Consumes = new List<string>
                    {
                        "multipart/form-data"
                    },
                    Produces = new List<string>
                    {
                        "application/json"
                    },
                    Parameters = new List<IParameter>
                    {
                        new NonBodyParameter
                        {
                            Name = "grant_type",
                            In = "formData",
                            Description = "",
                            Required = true
                        },
                        new NonBodyParameter
                        {
                            Name = "client_id",
                            In = "formData",
                            Description = "",
                            Required = true
                        },
                        new NonBodyParameter
                        {
                            Name = "client_secret",
                            In = "formData",
                            Description = "",
                            Required = true
                        },
                        new NonBodyParameter
                        {
                            Name = "refresh_token",
                            In = "formData",
                            Description = "Refresh token value",
                            Required = false
                        },
                        new NonBodyParameter
                        {
                            Name = "username",
                            In = "formData",
                            Description = "",
                            Required = false
                        },
                        new NonBodyParameter
                        {
                            Name = "password",
                            In = "formData",
                            Description = "",
                            Required = false
                        },
                        new NonBodyParameter
                        {
                            Name = "scope",
                            In = "formData",
                            Description = "",
                            Required = false
                        }
                    },
                    Responses = new Dictionary<string, Response>()
                    {
                        {"200", new Response() { Description = "OK", Schema = new Schema()
                            { Type = "object", Properties = new Dictionary<string, Schema>()
                                {
                                    {"access_token",  new Schema() { Type = "string" }},
                                    {"expires_in",  new Schema() { Type = "string" }},
                                    {"token_type",  new Schema() { Type = "string" }},
                                    {"refresh_token",  new Schema() { Type = "string" }}
                                }
                            } }
                        },
                        {"400", new Response() { Description = "Failed", Schema = new Schema()
                            { Type = "object", Properties = new Dictionary<string, Schema>()
                                {
                                    {"error",  new Schema() { Type = "string" }},
                                    {"error_description",  new Schema() { Type = "string" }}
                                }
                            } }
                        }
                    }
                }
            };
        }

        private PathItem RevokeTokenPathItem()
        {
            return new PathItem
            {
                Post = new Operation
                {
                    Description = "Revoke token",
                    Tags = new List<string> { "Token" },
                    Consumes = new List<string>
                    {
                        "multipart/form-data"
                    },
                    Parameters = new List<IParameter>
                    {
                        new NonBodyParameter
                        {
                            Name = "token",
                            In = "formData",
                            Description = "",
                            Required = true
                        },
                        new NonBodyParameter
                        {
                            Name = "token_type_hint",
                            In = "formData",
                            Description = "",
                            Required = false
                        }
                    },
                    Responses = new Dictionary<string, Response>()
                    {
                        {"200", new Response() {Description = "OK", Schema = new Schema() { Type = "string" } } },
                        {"400", new Response() {Description = "Failed", Schema = new Schema()
                            { Type = "object", Properties = new Dictionary<string, Schema>()
                                {
                                    {"error",  new Schema() { Type = "string" }},
                                    {"error_description",  new Schema() { Type = "string" }}
                                }
                            } }
                        }
                    },
                    Security = new List<IDictionary<string, IEnumerable<string>>>()
                    {
                        new Dictionary<string, IEnumerable<string>>()
                        {
                            { "basic", new string[] { } }
                        }
                    }

                }
            };
        }
    }
}
