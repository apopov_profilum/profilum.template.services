﻿using System;
using System.Net;

namespace Template.Services.Core.Exceptions
{
    /// <summary>
    /// Exception with StatusCode
    /// </summary>
    public class HttpStatusCodeException : Exception
    {
        /// <summary>
        /// Http Code 
        /// </summary>
        public HttpStatusCode StatusCode { get; set; }

        public HttpStatusCodeException(HttpStatusCode statusCode, string message) : base(message)
        {
            StatusCode = statusCode;
        }
    }
}
