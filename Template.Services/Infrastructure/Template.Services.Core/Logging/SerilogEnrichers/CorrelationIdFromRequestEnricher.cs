﻿using Serilog.Core;
using Serilog.Events;
using System;

namespace Template.Services.Core.Logging.SerilogEnrichers
{
    /// <summary>
    /// Serilog output template {CorrelationId} element 
    /// </summary>
    internal class CorrelationIdFromRequestEnricher : ILogEventEnricher
    {
        private readonly Guid _correlationId;

        private const string TagName = "CorrelationId";

        public CorrelationIdFromRequestEnricher(Guid correlationId)
        {
            _correlationId = correlationId;
        }

        public void Enrich(LogEvent logEvent, ILogEventPropertyFactory propertyFactory)
        {
            logEvent.AddPropertyIfAbsent(propertyFactory.CreateProperty(TagName, _correlationId));
        }
    }
}
