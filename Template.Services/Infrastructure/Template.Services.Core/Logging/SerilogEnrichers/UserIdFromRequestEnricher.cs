﻿using System;
using Serilog.Events;
using Serilog.Core;

namespace Template.Services.Core.Logging.SerilogEnrichers
{
    /// <summary>
    /// Serilog output template {UserId} element 
    /// </summary>
    public class UserIdFromRequestEnricher : ILogEventEnricher
    {
        private readonly Guid _correlationId;

        private const string TagName = "CorrelationId";

        public UserIdFromRequestEnricher(Guid correlationId)
        {
            _correlationId = correlationId;
        }

        public void Enrich(LogEvent logEvent, ILogEventPropertyFactory propertyFactory)
        {
            logEvent.AddPropertyIfAbsent(propertyFactory.CreateProperty(TagName, _correlationId));
        }
    }
}
