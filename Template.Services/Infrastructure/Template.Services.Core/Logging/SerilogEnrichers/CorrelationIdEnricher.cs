﻿using Serilog.Events;
using Serilog.Core;
using CorrelationId;

namespace Template.Services.Core.Logging.SerilogEnrichers
{
    /// <summary>
    /// Serilog output template {CorrelationId} element 
    /// </summary>
    public class CorrelationIdEnricher : ILogEventEnricher
    {
        private readonly ICorrelationContextAccessor _correlationContextAccessor;

        private const string TagName = "CorrelationId";

        public CorrelationIdEnricher(ICorrelationContextAccessor correlationContextAccessor)
        {
            _correlationContextAccessor = correlationContextAccessor;
        }

        public void Enrich(LogEvent logEvent, ILogEventPropertyFactory propertyFactory)
        {
            logEvent.AddPropertyIfAbsent(propertyFactory.CreateProperty(TagName, _correlationContextAccessor.CorrelationContext?.CorrelationId ?? string.Empty));
        }
    }
}
