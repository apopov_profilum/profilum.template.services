﻿using System;
using Serilog;
using Template.Services.Core.Logging.SerilogEnrichers;

namespace Template.Services.Core.Logging.Extensions
{
    public static class LoggerExtensions
    {
        /// <summary>
        /// Write a log event with the Serilog.Events.LogEventLevel.Fatal level with extra fields
        /// </summary>
        /// <param name="logger"></param>
        /// <param name="correlationId">correlation Id</param>
        /// <param name="message">message text</param>
        public static void Fatal(this ILogger logger, Guid correlationId, Guid userId, string message)
        {
            logger.ForContext(new CorrelationIdFromRequestEnricher(correlationId))
                  .ForContext(new UserIdFromRequestEnricher(userId))
                  .Fatal(message);
        }

        /// <summary>
        /// Write a log event with the Serilog.Events.LogEventLevel.Error level with extra fields
        /// </summary>
        /// <param name="logger"></param>
        /// <param name="correlationId">correlation Id</param>
        /// <param name="message">message text</param>
        public static void Error(this ILogger logger, Guid correlationId, Guid userId, string message)
        {
            logger.ForContext(new CorrelationIdFromRequestEnricher(correlationId))
                  .ForContext(new UserIdFromRequestEnricher(userId))
                  .Error(message);
        }

        /// <summary>
        /// Write a log event with the Serilog.Events.LogEventLevel.Warning level with extra fields
        /// </summary>
        /// <param name="logger"></param>
        /// <param name="correlationId">correlation Id</param>
        /// <param name="message">message text</param>
        public static void Warning(this ILogger logger, Guid correlationId, Guid userId, string message)
        {
            logger.ForContext(new CorrelationIdFromRequestEnricher(correlationId))
                  .ForContext(new UserIdFromRequestEnricher(userId))
                  .Warning(message);
        }

        /// <summary>
        /// Write a log event with the Serilog.Events.LogEventLevel.Information level with extra fields
        /// </summary>
        /// <param name="logger"></param>
        /// <param name="correlationId">correlation Id</param>
        /// <param name="message">message text</param>
        public static void Information(this ILogger logger, Guid correlationId, Guid userId, string message)
        {
            logger.ForContext(new CorrelationIdFromRequestEnricher(correlationId))
                  .ForContext(new UserIdFromRequestEnricher(userId))
                  .Information(message);
        }

        /// <summary>
        /// Write a log event with the Serilog.Events.LogEventLevel.Debug level with extra fields
        /// </summary>
        /// <param name="logger"></param>
        /// <param name="correlationId">correlation Id</param>
        /// <param name="message">message text</param>
        public static void Debug(this ILogger logger, Guid correlationId, Guid userId, string message)
        {
            logger.ForContext(new CorrelationIdFromRequestEnricher(correlationId))
                  .ForContext(new UserIdFromRequestEnricher(userId))
                  .Debug(message);
        }
    }
}
