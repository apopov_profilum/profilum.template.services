﻿using System.Net.Http;
using System.Threading.Tasks;
using CorrelationId;

namespace Template.Services.Core.Logging.Handlers
{
    /// <summary>
    /// Класс для передачи correlation идентификатора в заголовке запроса 
    /// </summary>
    public class CorrelationHandler : DelegatingHandler
    {
        private const string CorrelationHeaderName = "X-Correlation-ID";

        private readonly ICorrelationContextAccessor _correlationContextAccessor;

        public CorrelationHandler(ICorrelationContextAccessor correlationContextAccessor)
        {
            _correlationContextAccessor = correlationContextAccessor;
        }

        protected override Task<HttpResponseMessage> SendAsync(
        HttpRequestMessage request, System.Threading.CancellationToken cancellationToken)
        {
            var correlationId = _correlationContextAccessor.CorrelationContext?.CorrelationId ?? null;
            if (!string.IsNullOrEmpty(correlationId))
            {
                request.Headers.Add(CorrelationHeaderName, correlationId);
            }

            return base.SendAsync(request, cancellationToken);
        }
    }
}
