﻿using System.ComponentModel.DataAnnotations;

namespace Template.Services.Dtos.Attributes
{
    public class RoleValidation
    {
        public static ValidationResult RoleValidate(string role)
        {
            switch (role)
            {
                case "admin":
                case "manager":
                case "user":
                    {
                        return ValidationResult.Success;
                    }
                default:
                    {
                        return new ValidationResult("Unsupported role");
                    }
            }
        }
    }
}
