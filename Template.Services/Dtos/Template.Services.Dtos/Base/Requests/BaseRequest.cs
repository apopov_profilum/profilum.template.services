﻿using EasyNetQ.MetaData.Abstractions;
using System;
using System.Runtime.Serialization;

namespace Template.Services.Dtos.Base.Requests
{
    public abstract class BaseRequest
    {
        [MessageProperty(Property.CorrelationId), IgnoreDataMember]
        public Guid CorrelationId { get; set; }

        [IgnoreDataMember]
        public Guid CorrelationUserId { get; set; }
    }
}
