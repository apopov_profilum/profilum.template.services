﻿using System.Reflection;

namespace Template.Services.Dtos.Base.Responses
{
    public abstract class BaseResponse
    {
        public string Status { get; set; } = string.Empty;
        public string Comment { get; set; } = string.Empty;
        public string SenderService { get; set; } = Assembly.GetEntryAssembly().GetName().Name;
    }
}
