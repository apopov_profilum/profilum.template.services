﻿using System;

namespace Template.Services.Dtos.MongoDb.Dto
{
    public class MongoDocumentDto
    {
        public Guid Id { get; set; }

        public string Name { get; set; }

        public string Description { get; set; }

        public DateTime CreatedDate { get; set; }
    }
}
