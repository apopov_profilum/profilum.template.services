﻿using Template.Services.Dtos.Base.Responses;
using System;

namespace Template.Services.Dtos.MongoDb.Responses
{
    public class CreateMongoDocumentResponse : BaseResponse
    {
        public Guid Id { get; set; }
    }
}
