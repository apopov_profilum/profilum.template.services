﻿using Template.Services.Dtos.Base.Responses;
using Template.Services.Dtos.MongoDb.Dto;
using System.Collections.Generic;

namespace Template.Services.Dtos.MongoDb.Responses
{
    public class GetMongoDocumentsResponse : BaseResponse
    {
        public IList<MongoDocumentDto> Documents { get; set; }
    }
}
