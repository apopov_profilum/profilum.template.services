﻿using Template.Services.Dtos.Base.Responses;
using Template.Services.Dtos.MongoDb.Dto;

namespace Template.Services.Dtos.MongoDb.Responses
{
    public class GetMongoDocumentResponse : BaseResponse
    {
        public MongoDocumentDto Document { get; set; }
    }
}
