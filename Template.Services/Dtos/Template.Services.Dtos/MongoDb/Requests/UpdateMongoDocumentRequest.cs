﻿using Template.Services.Dtos.Base.Requests;
using Template.Services.Dtos.MongoDb.Dto;

namespace Template.Services.Dtos.MongoDb.Requests
{
    public class UpdateMongoDocumentRequest : BaseRequest
    {
        public MongoDocumentDto Document { get; set; }
    }
}
