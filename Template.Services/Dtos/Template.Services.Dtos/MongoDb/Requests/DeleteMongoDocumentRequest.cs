﻿using Template.Services.Dtos.Base.Requests;
using System;

namespace Template.Services.Dtos.MongoDb.Requests
{
    public class DeleteMongoDocumentRequest : BaseRequest
    {
        public Guid Id { get; set; }
    }
}
