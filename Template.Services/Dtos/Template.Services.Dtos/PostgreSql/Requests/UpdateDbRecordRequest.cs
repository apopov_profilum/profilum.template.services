﻿using Template.Services.Dtos.Base.Requests;
using Template.Services.Dtos.PostgreSql.Dto;

namespace Template.Services.Dtos.PostgreSql.Requests
{
    public class UpdateDbRecordRequest : BaseRequest
    {
        public DbRecordDto Record { get; set; }
    }
}
