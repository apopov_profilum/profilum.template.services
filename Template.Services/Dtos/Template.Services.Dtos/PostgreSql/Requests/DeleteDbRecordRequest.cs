﻿using System;
using Template.Services.Dtos.Base.Requests;

namespace Template.Services.Dtos.PostgreSql.Requests
{
    public class DeleteDbRecordRequest : BaseRequest
    {
        public Guid Id { get; set; }
    }
}
