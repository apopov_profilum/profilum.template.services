﻿using System;
using Template.Services.Dtos.Base.Requests;

namespace Template.Services.Dtos.PostgreSql.Requests
{
    public class GetDbRecordRequest : BaseRequest
    {
        public Guid Id { get; set; }
    }
}
