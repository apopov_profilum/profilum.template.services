﻿using Template.Services.Dtos.Base.Responses;
using Template.Services.Dtos.PostgreSql.Dto;

namespace Template.Services.Dtos.PostgreSql.Responses
{
    public class GetDbRecordResponse : BaseResponse
    {
        public DbRecordDto Record { get; set; }
    }
}
