﻿using System;
using Template.Services.Dtos.Base.Responses;

namespace Template.Services.Dtos.PostgreSql.Responses
{
    public class CreateDbRecordResponse : BaseResponse
    {
        public Guid Id { get; set; }
    }
}
