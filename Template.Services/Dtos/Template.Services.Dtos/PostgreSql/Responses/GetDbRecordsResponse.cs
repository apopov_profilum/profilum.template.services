﻿using System.Collections.Generic;
using Template.Services.Dtos.Base.Responses;
using Template.Services.Dtos.PostgreSql.Dto;

namespace Template.Services.Dtos.PostgreSql.Responses
{
    public class GetDbRecordsResponse : BaseResponse
    {
        public IList<DbRecordDto> Records { get; set; }
    }
}
