﻿using System;

namespace Template.Services.Dtos.PostgreSql.Dto
{
    public class DbRecordDto
    {
        public Guid Id { get; set; }

        public string Name { get; set; }

        public string Description { get; set; }

        public DateTime CreatedDate { get; set; }
    }
}
