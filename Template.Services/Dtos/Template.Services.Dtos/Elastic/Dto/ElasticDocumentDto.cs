﻿using System;

namespace Template.Services.Dtos.Elastic.Dto
{
    public class ElasticDocumentDto
    {
        public Guid Id { get; set; }

        public string Name { get; set; }

        public string Description { get; set; }

        public DateTime CreatedDate { get; set; }
    }
}
