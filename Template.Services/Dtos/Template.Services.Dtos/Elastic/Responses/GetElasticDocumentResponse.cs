﻿using Template.Services.Dtos.Base.Responses;
using Template.Services.Dtos.Elastic.Dto;

namespace Template.Services.Dtos.Elastic.Responses
{
    public class GetElasticDocumentResponse : BaseResponse
    {
        public ElasticDocumentDto Document { get; set; }
    }
}
