﻿using System.Collections.Generic;
using Template.Services.Dtos.Base.Responses;
using Template.Services.Dtos.Elastic.Dto;

namespace Template.Services.Dtos.Elastic.Responses
{
    public class GetElasticDocumentsResponse : BaseResponse
    {
        public IList<ElasticDocumentDto> Documents { get; set; }
    }
}
