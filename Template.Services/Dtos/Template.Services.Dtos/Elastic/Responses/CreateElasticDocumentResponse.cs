﻿using System;
using Template.Services.Dtos.Base.Responses;

namespace Template.Services.Dtos.Elastic.Responses
{
    public class CreateElasticDocumentResponse : BaseResponse
    {
        public Guid Id { get; set; }
    }
}
