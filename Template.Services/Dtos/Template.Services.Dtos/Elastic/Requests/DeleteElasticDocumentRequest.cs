﻿using System;
using Template.Services.Dtos.Base.Requests;

namespace Template.Services.Dtos.Elastic.Requests
{
    public class DeleteElasticDocumentRequest : BaseRequest
    {
        public Guid Id { get; set; }
    }
}
