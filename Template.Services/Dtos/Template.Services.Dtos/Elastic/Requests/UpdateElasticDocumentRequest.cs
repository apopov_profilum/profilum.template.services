﻿using Template.Services.Dtos.Base.Requests;
using Template.Services.Dtos.Elastic.Dto;

namespace Template.Services.Dtos.Elastic.Requests
{
    public class UpdateElasticDocumentRequest : BaseRequest
    {
        public ElasticDocumentDto Document { get; set; }
    }
}
