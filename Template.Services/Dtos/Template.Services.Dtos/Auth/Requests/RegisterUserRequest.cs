﻿using Template.Services.Dtos.Base.Requests;

namespace Template.Services.Dtos.Auth.Requests
{
    public class RegisterUserRequest: BaseRequest
    {
        public string UserName { get; set; }

        public string Email { get; set; }

        public string Role { get; set; }

        public string Password { get; set; }

        public string ConfirmPassword { get; set; }
    }
}
