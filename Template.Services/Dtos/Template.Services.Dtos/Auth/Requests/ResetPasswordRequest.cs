﻿using Template.Services.Dtos.Base.Requests;

namespace Template.Services.Dtos.Auth.Requests
{
    public class ResetPasswordRequest : BaseRequest
    {
        public string Email { get; set; }

        public string Password { get; set; }

        public string PasswordResetToken { get; set; }
    }
}
