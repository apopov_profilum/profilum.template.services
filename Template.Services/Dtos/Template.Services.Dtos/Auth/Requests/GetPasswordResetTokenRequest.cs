﻿using Template.Services.Dtos.Base.Requests;

namespace Template.Services.Dtos.Auth.Requests
{
    public class GetPasswordResetTokenRequest : BaseRequest
    {
        public string Email { get; set; }
    }
}
