﻿using Template.Services.Dtos.Base.Requests;
using System;

namespace Template.Services.Dtos.Auth.Requests
{
    public class ChangePasswordRequest : BaseRequest
    {
        public Guid UserId { get; set; }

        public string ClientId { get; set; }

        public string CurrentPassword { get; set; }

        public string NewPassword { get; set; }
    }
}
