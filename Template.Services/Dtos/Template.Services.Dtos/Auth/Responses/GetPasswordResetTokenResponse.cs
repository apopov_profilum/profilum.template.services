﻿using Template.Services.Dtos.Base.Responses;

namespace Template.Services.Dtos.Auth.Responses
{
    public class GetPasswordResetTokenResponse : BaseResponse
    {
        public string PasswordResetToken { get; set; }
    }
}
